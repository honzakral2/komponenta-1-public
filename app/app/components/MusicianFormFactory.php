<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\MusiciansModel,
    App\Utils\ImageHandler;


class MusicianFormFactory extends Nette\Object
{
    /** @var MusiciansModel */
    private $musiciansModel;

    const IMAGE_WIDTH = 100;
    const IMAGE_HEIGHT = 100;


    public function __construct(MusiciansModel $musiciansModel)
    {
        $this->musiciansModel = $musiciansModel;
    }


    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_pages');
        $form->addHidden('id_musicians');
        $form->addText('name', 'Jméno')
            ->setRequired('Zadejte prosím jméno.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka jména je %d znaků.', 255);
        $form->addText('instrument', 'Nástroj')
            ->setRequired('Zadejte prosím nástroj.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka nástroje je %d znaků.', 255);
        $form->addUpload(MusiciansModel::IMAGE_COLUMN_NAME, 'Fotka (nepovinné) | minimální rozměry: ' . self::IMAGE_WIDTH . 'x' . self::IMAGE_HEIGHT)
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, 'Fotka musí být JPG, PNG nebo GIF.');
        
        $form->addSubmit('submit', 'Uložit člena');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        $form->onSuccess[] = array($this, 'musicianSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function musicianSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_musicians'] != null) {

                $musician = $this->musiciansModel->getMusician($values['id_musicians']);

                //existuje citace, kterou chci zmenit?
                if ($musician) {
                    $values['title'] = $values['name'];
                    unset($values['id_pages']);

                    $this->createThumbnail($form, $values, $values['id_musicians']);

                    $this->musiciansModel->updateMusician($values, $values['id_musicians']);

                    $form->getParent()->redrawControl('musiciansList');
                    $form->getPresenter()->flashMessage('Údaje o členovi byly úspěšně upraveny.');
                }
            } else {
                $values['title'] = $values['name'];

                $this->createThumbnail($form, $values, $values['id_musicians']);

                $this->musiciansModel->addMusician($values);

                $form->getParent()->redrawControl('musiciansWrapper');
                $form->getPresenter()->flashMessage('Člen byl úspěšně přidán.');
            }
            $form->getParent()->redrawControl('musicianFormSnippet');
        } else {
            $form->getPresenter()->redirect('this');
        }
    }


    private function createThumbnail($form, $values, $id)
    {
        if ($values[MusiciansModel::IMAGE_COLUMN_NAME]->getError() != 4) {
            $imageDir = $this->musiciansModel->getImagesDir($id);
            $imageHandler = new ImageHandler($form, $imageDir, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);

            $values[MusiciansModel::IMAGE_COLUMN_NAME] =
                $imageHandler->getImageName($values[MusiciansModel::IMAGE_COLUMN_NAME]);

        } elseif ($id) {
            unset($values[MusiciansModel::IMAGE_COLUMN_NAME]);
        }
    }
}
