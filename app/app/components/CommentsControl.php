<?php
namespace App\Components;

use Nette,
    App\Model\CommentsModel,
    Nette\Application\UI\Control;

class CommentsControl extends Control
{
    /** @var CommentsModel */
    private $commentsModel;

    /** @var CommentFormFactory */
    private $commentFormFactory;

    /** @var Nette\Http\SessionSection */
    private $sessionSection;

    /** @var int */
    private $idItems;


    public function __construct(CommentsModel $commentsModel, CommentFormFactory $commentFormFactory)
    {
        parent::__construct();
        $this->commentsModel = $commentsModel;
        $this->commentFormFactory = $commentFormFactory;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idItems = $presenter->getParameter('itemSlug');
        // vytvorim session sekci primo pro dany post
        $session = $presenter->getSession();
        $this->sessionSection = $session->getSection('post-' . $this->idItems);
    }


    public function render()
    {
        $this->template->addFilter('ago','Helpers::timeAgoInWords');

        $this->template->setFile(__DIR__ . '/templates/comments.latte');

        $commentsOrder = $this->sessionSection['commentsOrder'] === 1
                        ? CommentsModel::ORDER_BY_LIKES
                        : CommentsModel::ORDER_BY_DATE;

        if (!isset($this->template->comments)) {
            $this->template->comments = $this->commentsModel->getComments($this->idItems, $commentsOrder);
        }
        $this->template->commentsReplies = $this->commentsModel->getCommentsReplies($this->idItems);
        $this->template->sessionSection = $this->sessionSection;

        $values['id_items'] = $this->idItems;
        $this['commentForm']->setValues($values);

        $this->template->render();
    }

    /**
     * Da signal pro zmenu poradi komentaru.
     * @param $commentsOrder int
     */
    public function handleOrder($commentsOrder)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->sessionSection['commentsOrder'] = (int) $commentsOrder;
            $this->redrawControl('commentsWrapper');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Odstrani dany komentar.
     * @param $id int - ID komentare
     */
    public function handleDelete($id)
    {
        $comment = $this->commentsModel->commentExists($id);

        if ($this->getPresenter()->isAjax() && $comment) {
            $this->commentsModel->deleteComment($id);
            $this->redrawControl('commentsWrapper');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Skryje/zobrazi dany komentar.
     * @param $id int - ID komentare
     */
    public function handleSwitchHiding($id)
    {
        $comment = $this->commentsModel->getComment($id)->fetch();

        if ($this->getPresenter()->isAjax() && $comment) {

            $action = $comment->hidden
                ? CommentsModel::SHOW_COMMENT
                : CommentsModel::HIDE_COMMENT;

            $this->commentsModel->switchHidingComment($action, $id);
            
            $this->redrawControl('commentsWrapper');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Postara se o lajknuti daneho komentare.
     * Pokud by byl komentar dislajknuty, odstrani dislike.
     * @param $id int - ID komentare
     */
    public function handleLike($id)
    {
        $comment = $this->commentsModel->commentExists($id);

        if ($this->getPresenter()->isAjax() && $comment) {
            $like = &$this->sessionSection->likes[$id];
            $dislike = &$this->sessionSection->dislikes[$id];

            if ($like !== 1) { // pokud neni koment lajknuty, dam mu like
                $like = 1;
                $this->commentsModel->rateComment($id, CommentsModel::LIKE);

                if ($dislike === 1) { // pokud byl koment dislajknuty, odstranim dislike
                    $dislike = null;
                    $this->commentsModel->rateComment($id, CommentsModel::REMOVE_DISLIKE);
                }
            } else { // pokud je koment lajknuty, odstranim like
                $like = null;
                $this->commentsModel->rateComment($id, CommentsModel::REMOVE_LIKE);
            }

            $this->template->comments = $this->commentsModel->getComment($id)->fetchAll();
            $this->redrawControl('commentsArea');
            $this->redrawControl('commentsList');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Postara se o dislajknuti daneho komentare.
     * Pokud by byl komentar lajknuty, odstrani like.
     * @param $id int - ID komentare
     */
    public function handleDislike($id)
    {
        $comment = $this->commentsModel->commentExists($id);

        if ($this->getPresenter()->isAjax() && $comment) {
            $like = &$this->sessionSection->likes[$id];
            $dislike = &$this->sessionSection->dislikes[$id];

            if ($dislike !== 1) { // pokud neni koment dislajknuty, dam mu dislike
                $dislike = 1;
                $this->commentsModel->rateComment($id, CommentsModel::DISLIKE);

                if ($like === 1) { // pokud byl koment lajknuty, odstranim like
                    $like = null;
                    $this->commentsModel->rateComment($id, CommentsModel::REMOVE_LIKE);
                }
            } else { // pokud je koment dislajknuty, odstranim dislike
                $dislike = null;
                $this->commentsModel->rateComment($id, CommentsModel::REMOVE_DISLIKE);
            }

            $this->template->comments = $this->commentsModel->getComment($id)->fetchAll();
            $this->redrawControl('commentsArea');
            $this->redrawControl('commentsList');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Formular pro pridavani komentaru.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentCommentForm()
    {
        return $this->commentFormFactory->create();
    }

}