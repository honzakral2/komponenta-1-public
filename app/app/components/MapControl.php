<?php
namespace App\Components;

use Nette,
    App\Model\MapManager,
    Nette\Application\UI\Control;


class MapControl extends Control
{
    /** @var MapManager */
    private $mapManager;


    public function __construct(MapManager $mapManager)
    {
        parent::__construct();
        $this->mapManager = $mapManager;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/map.latte');

        $template->currentPlaceId = $this->mapManager->getCurrentPlaceId();

        $template->render();
    }

    public function handleSaveAddress($placeId)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->mapManager->saveAddressPlaceId($placeId);
            $this->getPresenter()->flashMessage('Adresa byla uložena.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

}