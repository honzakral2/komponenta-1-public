<?php
namespace App\Components;

use Nette,
    App\Model\ReferencesModel,
    App\Model\QuotationsModel,
    Nette\Application\UI\Control;

// TODO Rozdelit na dve komponenty
class ReferencesControl extends Control
{
    /** @var ReferencesModel */
    private $referenceModel;

    /** @var QuotationsModel */
    private $quotationModel;

    /** @var QuotationFormFactory */
    private $quotationFormFactory;

    /** @var ReferenceFormFactory */
    private $referenceFormFactory;

    /** @var IPaginatorControl */
    private $paginatorControl;


    private $idPages = null;
    private $idReferences = null;
    private $idQuotations = null;
    private $displayReferenceForm = false;
    private $displayQuotationForm = false;

    // konstanty pro nastaveni paginatoru
    const ITEMS_PER_PAGE_QUOTATIONS = 8;
    const ITEMS_PER_PAGE_REFERENCES = 5;
    const MORE_BUTTON_TEXT_QUOTATIONS = 'Další citace';
    const MORE_BUTTON_TEXT_REFERENCES = 'Další články';


    /** @var int @persistent */
    public $quotationPageNumber = 1;

    /** @var int @persistent */
    public $referencePageNumber = 1;


    public function __construct(
        ReferencesModel $referenceModel,
        QuotationsModel $quotationModel,
        QuotationFormFactory $quotationFormFactory,
        ReferenceFormFactory $referenceFormFactory,
        IPaginatorControl $paginatorControl
    ) {
        parent::__construct();
        $this->referenceModel = $referenceModel;
        $this->quotationModel = $quotationModel;
        $this->quotationFormFactory = $quotationFormFactory;
        $this->referenceFormFactory = $referenceFormFactory;
        $this->paginatorControl = $paginatorControl;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }

    public function render()
    {
        $template = $this->template;

        $template->setFile(__DIR__ . '/templates/references.latte');

        $quotationsCount = $this->quotationModel->getQuotationsCount();
        /** @var PaginatorControl $paginatorControl */
        $paginatorQuotations = $this['paginatorQuotations'];
        $paginatorQuotations = $paginatorQuotations->getPaginator($quotationsCount);
        $template->quotations = $this->quotationModel->getQuotations($paginatorQuotations);

        $referencesCount = $this->referenceModel->getReferencesCount();
        /** @var PaginatorControl $paginatorControl */
        $paginatorReferences = $this['paginatorReferences'];
        $paginatorReferences = $paginatorReferences->getPaginator($referencesCount);
        $template->references = $this->referenceModel->getReferences($paginatorReferences);

        $template->displayReferenceForm = $this->displayReferenceForm;
        $template->displayQuotationForm = $this->displayQuotationForm;
        $template->idReferences = $this->idReferences;
        $template->idQuotations = $this->idQuotations;

        $template->render();
    }


    /********************* INSERT, UPDATE A DELETE OPERACE PRO REFERENCE *********************/

    public function handleDeleteReference($idReferences)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->referenceModel->deleteReference($idReferences);
            $this->redrawControl('referencesWrapper');
            $this->getPresenter()->flashMessage('Zvolený článek byl úspěšně odstraněn.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdateReference($idReferences)
    {
        $reference = $this->referenceModel->getReference($idReferences);

        if ($this->getPresenter()->isAjax() && $reference) {

            $date = new Nette\Utils\DateTime($reference['date']);
            $reference['date'] = $date->format('d.m.Y');
            $this['referenceForm']->setValues($reference);

            $this->idReferences = $idReferences;
            $this->displayReferenceForm = true;
            $this->redrawControl('referenceFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAddReference()
    {
        if ($this->getPresenter()->isAjax()) {
            $reference = array();
            $reference['id_pages'] = $this->idPages;
            $this['referenceForm']->setValues($reference);

            $this->displayReferenceForm = true;
            $this->redrawControl('referenceFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }
    
    /********************* INSERT, UPDATE A DELETE OPERACE PRO CITACE *********************/

    public function handleDeleteQuotation($idQuotations)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->quotationModel->deleteQuotation($idQuotations);
            $this->redrawControl('quotationsWrapper');
            $this->getPresenter()->flashMessage('Zvolená citace byla úspěšně odstraněna.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdateQuotation($idQuotations)
    {
        $quotation = $this->quotationModel->getQuotation($idQuotations);

        if ($this->getPresenter()->isAjax() && $quotation) {
            $this['quotationForm']->setValues($quotation);

            $this->idQuotations = $idQuotations;
            $this->displayQuotationForm = true;
            $this->redrawControl('quotationFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAddQuotation()
    {
        if ($this->getPresenter()->isAjax()) {
            $quotation = array();
            $quotation['id_pages'] = $this->idPages;
            $this['quotationForm']->setValues($quotation);

            $this->displayQuotationForm = true;
            $this->redrawControl('quotationFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }
    
    
    
    
    /********************* PŘIPOJENÍ KOMPONENT *********************/

    protected function createComponentReferenceForm()
    {
        return $this->referenceFormFactory->create();
    }
    
    protected function createComponentQuotationForm()
    {
        return $this->quotationFormFactory->create();
    }


    protected function createComponentPaginatorReferences()
    {
        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT_REFERENCES);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE_REFERENCES);
        $paginator->setSnippetsNames(['referencesContainer']);

        return $paginator;
    }
    
    protected function createComponentPaginatorQuotations()
    {
        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT_QUOTATIONS);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE_QUOTATIONS);
        $paginator->setSnippetsNames(['quotationsContainer']);

        return $paginator;
    }
}