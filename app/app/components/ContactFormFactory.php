<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\ContactsModel;


class ContactFormFactory extends Nette\Object
{
    /** @var ContactsModel */
    private $contactsModel;

    public function __construct(ContactsModel $contactsModel)
    {
        $this->contactsModel = $contactsModel;
    }

    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";
        
        $form->addHidden('id_pages');
        $form->addHidden('id_contacts');
        $form->addText('name', 'Jméno')
            ->setRequired('Zadejte prosím jméno.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka jména je %d znaků.', 45);
        $form->addText('function', 'Funkce')
            ->setRequired(FALSE)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka funkce je %d znaků.', 45);
        $form->addText('mail', 'E-mail')
            ->setRequired(FALSE)
            ->addRule(Form::EMAIL, 'E-mail je ve špatném formátu.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka e-mailu je %d znaků.', 45);
        $form->addText('phone', 'Telefon')
            ->setRequired(FALSE)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka telefonu je %d znaků.', 20);
        
        $form->addSubmit('submit', 'Uložit kontakt');

        $form->onSuccess[] = array($this, 'contactSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function contactSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_contacts'] != null) {

                $contact = $this->contactsModel->getContact($values['id_contacts']);

                //existuje kontakt, kterou chci zmenit?
                if ($contact) {
                    $values['title'] = $values['name'];
                    unset($values['id_pages']);

                    $this->contactsModel->updateContact($values, $values['id_contacts']);

                    $form->getParent()->redrawControl('contactsList');
                    $form->getPresenter()->flashMessage('Kontakt byl úspěšně upraven.');
                }
            } else {
                $values['title'] = $values['name'];

                $this->contactsModel->addContact($values);

                $form->getParent()->redrawControl('contactsWrapper');
                $form->getPresenter()->flashMessage('Kontakt byl úspěšně přidán.');
            }
            $form->getParent()->redrawControl('contactFormSnippet');
        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}