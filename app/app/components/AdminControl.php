<?php

namespace App\Components;

use Nette,
    App\Model\AdminModel,
    Nette\Application\UI\Control;

class AdminControl extends Control
{
    /** @var AdminModel */
    private $adminModel;

    /** @var UserManagerFormFactory */
    private $userManagerFormFactory;

    /** @var PageFormFactory */
    private $pageFormFactory;

    /** var int @persistent */
    public $idPages = null;

    /** var int @persistent */
    public $actionType = 1;


    public function __construct(
        AdminModel $adminModel,
        UserManagerFormFactory $userManagerFormFactory,
        PageFormFactory $pageFormFactory
    ) {
        parent::__construct();
        $this->adminModel = $adminModel;
        $this->userManagerFormFactory = $userManagerFormFactory;
        $this->pageFormFactory = $pageFormFactory;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/admin.latte');

        $template->actionType = $this->actionType;
        $template->pages = $this->adminModel->getPages();
        $template->users = $this->adminModel->getUsers();

        //předání instance formuláře šabloně, kvůli invalidaci snippetu uvnitř formuláře
        //$this->template->_form = $this['pageForm'];

        $template->render();
    }

    public function handleAddPage()
    {
        if ($this->getPresenter()->isAjax()) {
            $this->actionType = 1;
            $this->redrawControl('pageFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdatePage($idPages)
    {
        $page = $this->adminModel->getPage($idPages);

        if ($this->getPresenter()->isAjax() && $page) {
            $this['pageForm']->setValues($page);

            if ($page['homepage']) {
                $this['pageForm']['homepage']->setAttribute('onclick', 'return false');
            }

            $this->idPages = $idPages;
            $this->actionType = 2;
            $this->redrawControl('pageFormSnippet');

        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    //smaže danou stránku. Nelze takto smazat domovskou stránku. Pokud smažu stránku, na které se zrovna nacházím, jsem přesměrován na hlavní stránku
    public function handleDeletePage($idPages)
    {
        $page = $this->adminModel->getPage($idPages);

        if ($this->getPresenter()->isAjax() && $page) {
            if (!$page['homepage']) {
                $this->adminModel->deletePage($idPages);
                $slug = $this->getPresenter()->getParameter('slug');
                //pokud chci smazat sekci, ve které se právě nacházím, provede se přesměrování na hlavní stránku
                if ($slug == $idPages) {
                    $idMainPage = $this->adminModel->getHomepageId();
                    $this->getPresenter()->redirect('admin', array('slug' => $idMainPage));
                } else {
                    $this->redrawControl('pagesWrapper');
                    $this->getPresenter()->redrawControl('sortArea');
                }
            }
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * Vrátí pomocí payloadu slug vytvořený ze zadaného parametru
     * @param $menuTitle string  titulek, ze ktereho se vytvori slug
     */
    /*public function handleGetSlug($menuTitle)
    {
        if ($this->getPresenter()->isAjax()) {
            $slug = Strings::webalize($menuTitle);
            $this->getPresenter()->payload->message = (object) array(
                'slug' => $slug,
            );
            $this->getPresenter()->sendPayload();
        } else {
            $this->getPresenter()->redirect('this');
        }
    }*/


    /**
     * Umoznuje razeni stranek/sekci pomocí AJAXu.
     * @param $idPage int  index stranky/sekce
     * @param $sort int  poradove cislo
     */
    public function handleSort($idPage, $sort)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->adminModel->setSort($idPage, $sort);
            $this->getPresenter()->redrawControl('sortArea');
            $this->getPresenter()->flashMessage('Pořadí sekcí v menu bylo úspěšně změněno!');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }


    protected function createComponentPageForm()
    {
        return $this->pageFormFactory->createPageForm();
    }


    /********************* SPRAVA UZIVATELU *********************/

    /**
     * @param $idUsers int
     * @throws Nette\Application\BadRequestException
     */
    public function handleUpdateUser($idUsers)
    {
        // nejprve kontrola existence
        $user = $this->adminModel->getUser($idUsers);

        if ($this->getPresenter()->isAjax() && $user) {

            $this->template->username = $user->username;
            $this['updateUserForm']->setValues($user);
            $this->redrawControl('updateUserForm');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * @param $idUsers int
     * @throws Nette\Application\BadRequestException
     */
    public function handleUpdatePassword($idUsers)
    {
        $user = $this->adminModel->getUser($idUsers);

        if ($this->getPresenter()->isAjax() && $user) {

            $this->template->username = $user->username;
            $values['id_users'] = $idUsers;
            $this['updatePasswordForm']->setValues($values);
            $this->redrawControl('updatePasswordForm');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    /**
     * @param $idUsers int
     * @throws Nette\Application\BadRequestException
     */
    public function handleSwitchBlockingUser($idUsers)
    {
        $user = $this->adminModel->getUser($idUsers);

        if ($this->getPresenter()->isAjax() && $user) {

            $action = $user->blocked
                ? AdminModel::UNBLOCK_USER
                : AdminModel::BLOCK_USER;

            $this->adminModel->switchBlockingUser($action, $idUsers);

            $this->redrawControl('usersList');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }


    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentAddUserForm()
    {
        return $this->userManagerFormFactory->createAddUserForm();
    }

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentUpdateUserForm()
    {
        return $this->userManagerFormFactory->createUpdateUserForm();
    }

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentUpdatePasswordForm()
    {
        return $this->userManagerFormFactory->createUpdatePasswordForm();
    }
}
