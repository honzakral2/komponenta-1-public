<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\ConcertsModel;


class ConcertFormFactory extends Nette\Object
{
    /** @var ConcertsModel */
    private $concertModel;

    public function __construct(ConcertsModel $concertModel)
    {
        $this->concertModel = $concertModel;
    }


    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";
        
        $form->addHidden('id_concerts');
        $form->addHidden('id_pages');
        $form->addText('title', 'Název')
            ->setRequired('Zadejte prosím název.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu je %d znaků.', 255);
        $form->addText('title_en', 'Název (anglicky)')
            ->setRequired('Zadejte prosím název (anglicky).')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu (anglicky) je %d znaků.', 255);
        //$form->addHidden('slug');
        $form->addText('date', 'Datum')
            ->setRequired('Zadejte prosím datum konání akce.')
            ->setAttribute('placeholder', "dd.mm.rrrr")
            ->addRule(Form::PATTERN, 'Datum musí být ve tvaru dd.mm.rrrr, tedy např. 31.12.1992', "\d\d?\.?\d\d?\.?\d{4}");
        $form->addText('time', 'Čas')
            ->setAttribute('class', 'timepicker')
            ->setAttribute('placeholder', "hh:mm")
            ->setRequired('Zadejte prosím čas konání akce.')
            ->addRule(Form::PATTERN, 'Čas musí být ve tvaru hh:mm, tedy např. 10:00', "\d\d?:\d\d");
        $form->addText('place', 'Místo')
            ->setRequired('Zadejte prosím místo konání akce.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka místa je %d znaků.', 255);
        $form->addText('ticket_url', 'Odkaz na vstupenky (nepovinné)')
            ->setRequired(FALSE)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka odkazu na vstupenky je %d znaků.', 1000);
        $form->addTextArea('description', 'Popis')
            ->setRequired('Zadejte prosím popis koncertu.')
            ->setAttribute('class', 'tinyMCEconcerts');
        $form->addTextArea('description_en', 'Popis (anglicky)')
            ->setRequired('Zadejte prosím popis koncertu (anglicky).')
            ->setAttribute('class', 'tinyMCEconcerts');
        
        $form->addSubmit('submit', 'Uložit koncert');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        foreach ($form->getComponents(TRUE, 'SubmitButton') as $button) {
            if (!$button->getValidationScope()) continue;
            $button->getControlPrototype()->onclick('tinyMCE.triggerSave()');
        }

        $form->onSuccess[] = array($this, 'concertSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function concertSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_concerts'] != null) {

                $concert = $this->concertModel->getConcert($values['id_concerts']);

                //existuje koncert, ktery chci zmenit?
                if ($concert) {
                    $values['date'] = new \DateTime($values['date'] . " " . $values['time'] . ":00");
                    unset($values['time']);
                    unset($values['id_pages']);
                    /*$values['slug'] = '0'.$values['id_concerts'].'-'.substr(Nette\Utils\Strings::webalize($values['title']), 0, 50);
                    $currentSlug = $concert['slug'];
                    $currentPostSlug = $this->concertModel->getConcertSlug($form->getPresenter()->getParameter('postSlug'));*/

                    $this->concertModel->updateConcert($values, $values['id_concerts']);

                    //pokud se nachazim na strance (prispevku), ktery zrovna upravuji, a upravim jeho slug, musim provest presmerovani
                    /*if ($currentPostSlug == $currentSlug && $currentSlug != $values['slug']) {
                        $form->getPresenter()->redirect('this', array('postSlug' => $values['slug']));
                    } else {*/

                        $form->getParent()->redrawControl('futureConcertsList');
                        $form->getParent()->redrawControl('pastConcertsList');
                        $form->getParent()->redrawControl('concertWrapper');

                        $form->getPresenter()->redrawControl('commentsControl');

                        $form->getPresenter()->flashMessage('Koncert byl úspěšně upraven.');
                    //}
                }
            } else {
                $values['comments_allowed'] = true;
                $values['date'] = new \DateTime($values['date'] . " " . $values['time'] . ":00");
                unset($values['time']);
                
                $this->concertModel->addConcert($values);

                $form->getParent()->redrawControl('concertsWrapper');
                $form->getPresenter()->flashMessage('Koncert byl úspěšně přidán.');
            }
            $form->getParent()->redrawControl('concertFormSnippet');
        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}
