<?php
namespace App\Components;

use Nette,
    App\Model\MusiciansModel,
    Nette\Application\UI\Control;


class MusiciansControl extends Control
{
    /** @var MusiciansModel */
    private $musiciansModel;

    /** @var MusicianFormFactory */
    private $musicianFormFactory;

    private $idPages = null;
    private $idMusicians = null;
    private $displayMusicianForm = false;


    public function __construct(MusiciansModel $musiciansModel, MusicianFormFactory $musicianFormFactory)
    {
        parent::__construct();
        $this->musiciansModel = $musiciansModel;
        $this->musicianFormFactory = $musicianFormFactory;
    }


    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }


    public function render()
    {
        $template = $this->template;

        $template->setFile(__DIR__ . '/templates/musicians.latte');

        $template->musicians = $this->musiciansModel->getMusicians();
        $template->displayMusicianForm = $this->displayMusicianForm;
        $template->idMusicians = $this->idMusicians;

        $template->render();
    }


    /********************* INSERT, UPDATE A DELETE OPERACE *********************/

    public function handleDelete($idMusicians)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->musiciansModel->deleteMusician($idMusicians);
            $this->redrawControl('musiciansWrapper');
            $this->getPresenter()->flashMessage('Zvolený člen byl úspěšně odstraněn.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdate($idMusicians)
    {
        $musician = $this->musiciansModel->getMusician($idMusicians);

        if ($this->getPresenter()->isAjax() && $musician) {
            $this['musicianForm']->setValues($musician);
            $this->idMusicians = $idMusicians;
            $this->displayMusicianForm = true;
            $this->redrawControl('musicianFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAdd()
    {
        if ($this->getPresenter()->isAjax()) {
            $musician = array();

            $musician['id_pages'] = $this->idPages;
            $this['musicianForm']->setValues($musician);

            $this->displayMusicianForm = true;
            $this->redrawControl('musicianFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }


    /**
     * @param $idMusicians int  id clena
     * @param $sort int  poradove cislo
     */
    public function handleSort($idMusicians, $sort)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->musiciansModel->setSort($idMusicians, $sort);
            $this->getPresenter()->flashMessage('Pořadí členů bylo úspěšně změněno!');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }


    protected function createComponentMusicianForm()
    {
        return $this->musicianFormFactory->create();
    }

}