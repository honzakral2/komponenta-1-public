<?php
namespace App\Components;

use Nette,
    App\Model\ContactsModel,
    Nette\Application\UI\Control;


class ContactsControl extends Control
{
    /** @var ContactsModel */
    private $contactsModel;

    /** @var MapControl */
    private $mapControl;

    /** @var ContactFormFactory */
    private $contactFormFactory;

    private $idPages = null;
    private $idContacts = null;
    private $displayContactForm = false;


    public function __construct(
        ContactsModel $contactsModel,
        ContactFormFactory $contactFormFactory,
        MapControl $mapControl
    ) {
        parent::__construct();
        $this->contactsModel = $contactsModel;
        $this->mapControl = $mapControl;
        $this->contactFormFactory = $contactFormFactory;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/contacts.latte');
        
        $template->contacts = $this->contactsModel->getContacts();
        $template->displayContactForm = $this->displayContactForm;
        $template->idContacts = $this->idContacts;

        $template->render();
    }

    /********************* INSERT, UPDATE A DELETE OPERACE *********************/

    public function handleDelete($idContacts)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->contactsModel->deleteContact($idContacts);
            $this->redrawControl('contactsWrapper');
            $this->getPresenter()->flashMessage('Zvolený kontakt byl úspěšně odstraněn.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdate($idContacts)
    {
        $contact = $this->contactsModel->getContact($idContacts);

        if ($this->getPresenter()->isAjax() && $contact) {
            $this['contactForm']->setValues($contact);
            $this->idContacts = $idContacts;
            $this->displayContactForm = true;
            $this->redrawControl('contactFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAdd()
    {
        if ($this->getPresenter()->isAjax()) {
            $contact = array();
            $contact['id_pages'] = $this->idPages;
            $this['contactForm']->setValues($contact);

            $this->displayContactForm = true;
            $this->redrawControl('contactFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }
    
    /**
     * @param $idContacts int  id kontaktu
     * @param $sort int  poradove cislo
     */
    public function handleSort($idContacts, $sort)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->contactsModel->setSort($idContacts, $sort);
            $this->getPresenter()->redrawControl('contactsList');
            $this->getPresenter()->flashMessage('Pořadí kontaktů bylo úspěšně změněno!');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    protected function createComponentContactForm()
    {
        return $this->contactFormFactory->create();
    }

    protected function createComponentMap()
    {
        return $this->mapControl;
    }

}