<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AlbumsModel,
    App\Utils;


class VideosFormFactory extends Nette\Object
{
    /** @var AlbumsModel */
    private $galleryModel;


    public function __construct(AlbumsModel $galleryModel)
    {
        $this->galleryModel = $galleryModel;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addHidden('id_albums');

        $form->addText('filename','Odkaz na YouTube video')
            ->setRequired('Zadejte prosím odkaz na video, které chcete vložit.')
            ->addRule(Form::PATTERN, 'Adresa byla zadána ve špatném tvaru!', "^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)");

        $form->addSubmit('submit', 'Přidat video do alba');
        $form->onSuccess[] = array($this,'uploadVideoSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values array
     */
    public function uploadVideoSucceeded($form, $values)
    {
        $values['filename'] = $this->parseYTid($values);

        $video = $this->galleryModel->getVideoByName($values['filename']);

        if (!$video) {
            $this->galleryModel->insertVideo($values);

            $form->getPresenter()->flashMessage('Video bylo úspěšně vloženo.');
        } else {
            $form->getPresenter()->flashMessage('Zadané video již v databázi existuje.', 'warning');
        }

        $form->getPresenter()->redirect('this');
    }

    private function parseYTid($values)
    {
        $regexp = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)$/";
        preg_match($regexp, $values['filename'], $matches);
        return $matches[1];
    }
}
