<?php

namespace App\Components;

use Nette,
    App\Model\AlbumsModel,
    Nette\Application\UI\Control;

class AlbumControl extends Control
{
    /** @var AlbumsModel */
    private $galleryModel;

    /** @var IPaginatorControl */
    private $paginatorControl;

    /** @var array */
    private $images;

    private $idAlbums;

    private $isStandAlone = false;


    // konstanty pro nastaveni paginatoru
    const ITEMS_PER_PAGE = 16;
    const MORE_BUTTON_TEXT = 'Další fotky';


    public function __construct(AlbumsModel $galleryModel, IPaginatorControl $paginatorControl)
    {
        parent::__construct();
        $this->galleryModel = $galleryModel;
        $this->paginatorControl = $paginatorControl;
    }

    // TODO Upravit - Umoznit vice alb na strance a umoznit oba typy (video i foto album). Vyuzit vice view komponenty? Multiplier?
    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        // zjisti, jestli je album na samostatne strance v podobě postu ...
        $itemSlug = $presenter->getParameter('itemSlug');
        if ($itemSlug)
            $this->idAlbums = $this->galleryModel->getAlbumsId($itemSlug, 'items');

        // ... nebo je soucasti page (neni mu nadrazena galerie)
        $slug = $presenter->getParameter('slug');
        if ($slug) {
            $this->isStandAlone = true;
            $albumsCount = $this->galleryModel->getAlbumsCount($slug);

            if ($albumsCount == 0) { // na strance jeste neni album => vytvori se nove album
                $album = [
                    'title' => 'album',
                    'id_pages' => $slug,
                    'id_users' => $presenter->getUser()->getId()
                ];
                $this->galleryModel->insertFotoAlbum($album);
                $this->idAlbums = $this->galleryModel->getAlbumsId($slug, 'pages');

            } elseif ($albumsCount == 1) { // na strance uz je album => preda se jeho ID
                $this->idAlbums = $this->galleryModel->getAlbumsId($slug, 'pages');

            } else { // na strance uz je vice alb => vyhodi se vyjimka
                throw(new Nette\Application\ApplicationException(
                    "More than one album on page (id_pages = $slug) - there can be only one. How about creating a gallery?"
                ));
            }
        }
    }


    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/album.latte');

        $this->template->isStandAlone = $this->isStandAlone;

        $album = $this->galleryModel->getAlbum($this->idAlbums);
        $this->template->album = $album;

        $photosCount = $this->galleryModel->getImagesCount($this->idAlbums);

        /** @var PaginatorControl $paginatorControl */
        $paginatorControl = $this['paginator'];
        $paginator = $paginatorControl->getPaginator($photosCount);

        $this->images = $this->galleryModel->getImages($this->idAlbums, $paginator);
        $this->template->images = $this->images;
        $this->template->videos = $this->images;

        $this['changeAlbumNameForm']->setValues($album);
        $this['uploadImagesForm']->setValues($album);
        $this['videosForm']->setValues($album);

        $template->render();
    }


    protected function createComponentRemoveImagesForm()
    {
        $control = new RemoveImagesFormFactory($this->galleryModel, $this->images);
        $form = $control->create();
        return $form;
    }

    protected function createComponentChangeAlbumNameForm()
    {
        $control = new ChangeAlbumNameFormFactory($this->galleryModel);
        $form = $control->create();
        return $form;
    }

    protected function createComponentUploadImagesForm()
    {
        $control = new UploadImagesFormFactory($this->galleryModel);
        $form = $control->create();
        return $form;
    }



    public function handleDelete($idVideo)
    {
        $video = $this->galleryModel->getVideo($idVideo);
        if ($video) {
        //if ($this->getPresenter()->isAjax()) {
            $this->galleryModel->deleteVideo($idVideo);
            //$this->redrawControl('postsWrapper');
            $this->getPresenter()->flashMessage('Zvolené video bylo úspěšně odstraněno.');
            $this->getPresenter()->redirect('this');
/*        } else {
            $this->getPresenter()->redirect('this');
        }*/
        } else {
            $this->getPresenter()->flashMessage('Zvolené video v databázi neexistuje.','warning');
        }
    }

    protected function createComponentVideosForm()
    {
        $control = new VideosFormFactory($this->galleryModel);
        $form = $control->create();
        return $form;
    }


    protected function createComponentPaginator()
    {
        $snippets = array();
        $snippets[] = 'imagesArea';
        $snippets[] = 'paginator';
        $snippets[] = 'paginator2';

        if ($this->getPresenter()->getView() === 'admin') {
            $snippets[] = 'photosContainer-Admin';
        } else {
            $snippets[] = 'photosContainer';
        }

        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE);
        $paginator->setSnippetsNames($snippets);

        return $paginator;
    }
}
