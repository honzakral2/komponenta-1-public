<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AlbumsModel;


class RemoveImagesFormFactory extends Nette\Object
{
    /** @var AlbumsModel */
    private $galleryModel;

    /** @var array */
    private $images;


    public function __construct(AlbumsModel $galleryModel, $images)
    {
        $this->galleryModel = $galleryModel;
        $this->images = $images;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addCheckboxList('images', '', $this->images);

        $form->addSubmit('submit', 'Odstranit zvolené fotky');

        $form->onSuccess[] = array($this,'removeImagesSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     */
    public function removeImagesSucceeded($form)
    {
        $images = $form->getHttpData($form::DATA_TEXT, 'images[]');

        $this->galleryModel->deleteImages($images);

        $form->getPresenter()->flashMessage('Zvolené fotky byly úspěšně odstraněny.');
        $form->getPresenter()->redirect('this');
    }

}
