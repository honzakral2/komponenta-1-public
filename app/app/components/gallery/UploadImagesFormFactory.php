<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AlbumsModel,
    App\Utils;


class UploadImagesFormFactory extends Nette\Object
{
    /** @var AlbumsModel */
    private $galleryModel;

    const IMAGE_WIDTH = 190;
    const IMAGE_HEIGHT = 190;


    public function __construct(AlbumsModel $galleryModel)
    {
        $this->galleryModel = $galleryModel;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addHidden('id_albums');

        $form->addHidden('title');

        $form->addMultiUpload('imgs','Zde vyberte požadované fotky | minimální rozměry: ' . self::IMAGE_WIDTH . 'x' . self::IMAGE_HEIGHT)
            ->setRequired('Nejdříve musíte zvolit obrázky, které chcete nahrát.')
            ->addCondition(Form::FILLED)
            ->addRule(Form::IMAGE, 'Obrázek musí být ve formátu JPG, PNG nebo GIF.');

        $form->addSubmit('submit', 'Přidat fotky do alba');
        $form->onSuccess[] = array($this,'uploadImagesSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values array
     */
    public function uploadImagesSucceeded($form, $values)
    {
        $imageDir = $this->galleryModel->getImagesDir($values['id_albums']);
        $imageAdjuster = new Utils\ImagesHandler($form, $imageDir, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        $imageAdjuster->avoidDuplication(true);
        $imageNames = $imageAdjuster->getImageNames($values['imgs']);

        $duplicatesCount = $imageAdjuster->getDuplicatesCount();
        $choosedImgsCount = count($values['imgs']);
        $uploadedImgsCount = count($imageNames);
        $notUploadedImgsCount = $form->hasErrors() ? ($choosedImgsCount - $uploadedImgsCount) : 0;

        $idItems = $this->galleryModel->getIdItems($values['id_albums']);
        $this->galleryModel->insertImages($idItems, $imageNames);

        $messageType = 'warning';

        if ($duplicatesCount == $choosedImgsCount) { // vsechny fotky jsou duplicitni
            $messageText = 'Zvolené fotky v albu již jsou. Nebyly tedy přidány.';

        } elseif ($duplicatesCount > 0) { // nektere jsou duplicitni
            $messageText = 'Některé z vkládaných fotek v albu již jsou (' . $duplicatesCount . ').';

            if (($notUploadedImgsCount - $duplicatesCount) > 0) // navic se nektere nemusi podarit nahrat
                $messageText .= ' Některé se nepodařilo nahrát (' . ($notUploadedImgsCount - $duplicatesCount) . ').';

            if ($notUploadedImgsCount === $choosedImgsCount) {
                $messageText .= ' Přidaná tedy nebyla žádná.';
            } else {
                $messageText .= ' Počet přidaných fotek: ' . ($uploadedImgsCount);
                $messageType = 'success';
            }

        } elseif ($notUploadedImgsCount === $choosedImgsCount) { // vsechny se nepodarilo nahrat
            $messageText = 'Vybrané fotky se nepodařilo nahrát.';

        } else { // zadna neni duplicitni
            if ($notUploadedImgsCount === 0) { // vsechny se podarilo nahrat
                $messageText = 'Vybrané fotky byly úspěšně přidány.';
                $messageType = 'success';
            } else { // nektere se nepodarilo nahrat
                $messageText = 'Některé fotky se nepodařilo nahrát (' . $notUploadedImgsCount . ').
                    Počet přidaných fotek: ' . $uploadedImgsCount;
            }
        }

        $form->getPresenter()->flashMessage($messageText, $messageType);

        // redirect jen kdyz neni chyba - aby se pripadne chyby vypsaly
        if (!$form->hasErrors()) {
            $form->getPresenter()->redirect('this');
        }
    }

}
