<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AlbumsModel;


class ChangeAlbumNameFormFactory extends Nette\Object
{
    /** @var AlbumsModel */
    private $galleryModel;


    public function __construct(AlbumsModel $galleryModel)
    {
        $this->galleryModel = $galleryModel;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_albums');

        $form->addText('title', 'Název alba')
            ->setRequired('Zadejte prosím název vytvářeného alba.')
            ->addRule(Form::MIN_LENGTH, 'Minimální délka nazvu alba jsou %d znaky.', 2)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka nazvu alba je %d znaků.', 60);

        $form->addTextArea('description', 'Popis alba (nepovinné)')
            ->addCondition(Form::FILLED)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka popisu alba je %d znaků.', 300);

        $form->addSubmit('submit', 'Uložit změny');
        $form->onSuccess[] = array($this,'changeAlbumNameSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values Nette\Utils\ArrayHash
     */
    public function changeAlbumNameSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {

            $album = $this->galleryModel->getAlbum($values['id_albums']);

            if ($album) {
                $this->galleryModel->updateAlbum($values);

                $form->getPresenter()->flashMessage('Úprava alba proběhla úspěšně.');
                $form->getParent()->redrawControl('albumsList');
                $form->getParent()->redrawControl('changeNameFormSnippet');

                $form->getParent()->redrawControl('albumsName');
            }
        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}
