<?php

namespace App\Components;

use Nette,
    App\Model\AlbumsModel,
    Nette\Application\UI\Control;

class GalleryControl extends Control
{
    /** @var AlbumsModel */
    private $galleryModel;

    /** @var IPaginatorControl */
    private $paginatorControl;

    private $idPages = -1;
    private $displayChangeNameForm = false;
    private $displayAddAlbumForm = false;


    const ITEMS_PER_PAGE = 10;
    const MORE_BUTTON_TEXT = 'Další alba';

    /** @var int @persistent */
    public $pageNumber = 1;

    public function __construct(AlbumsModel $galleryModel, IPaginatorControl $paginatorControl)
    {
        parent::__construct();
        $this->galleryModel = $galleryModel;
        $this->paginatorControl = $paginatorControl;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }


    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/gallery.latte');

        $template->displayAddAlbumForm = $this->displayAddAlbumForm;
        $template->displayChangeNameForm = $this->displayChangeNameForm;

        $albumsCount = $this->galleryModel->getAlbumsCount($this->idPages);

        /** @var PaginatorControl $paginatorControl */
        $paginatorControl = $this['paginator'];
        $paginator = $paginatorControl->getPaginator($albumsCount);

        $template->albums = $this->galleryModel->getAlbums($this->idPages, $paginator);
        $template->previewImages = $this->galleryModel->getPreviewImages($this->idPages);

        $template->render();
    }

    public function handleAdd()
    {
        if ($this->getPresenter()->isAjax()) {
            $album = array();

            $album['id_pages'] = $this->idPages;
            $this['addAlbumForm']->setValues($album);

            $this->displayAddAlbumForm = true;
            $this->redrawControl('addAlbumFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleDelete($idAlbums)
    {
        $album = $this->galleryModel->getAlbum($idAlbums);

        if ($this->getPresenter()->isAjax() && $album) {

            $this->galleryModel->deleteAlbum($idAlbums);

            $this->pageNumber = 1;
            $this->redrawControl('galleryWrapper');
            $this->getPresenter()->flashMessage('Album bylo úspěšně odstraněno.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdate($idAlbums)
    {
        $album = $this->galleryModel->getAlbum($idAlbums);

        if ($this->getPresenter()->isAjax() && $album) {
            $this->displayChangeNameForm = true;
            $this['changeAlbumNameForm']->setValues($album);
            $this->redrawControl('changeNameFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }


    protected function createComponentAddAlbumForm()
    {
        $control = new AddAlbumFormFactory($this->galleryModel);
        $form = $control->create();
        return $form;
    }

    protected function createComponentChangeAlbumNameForm()
    {
        $control = new ChangeAlbumNameFormFactory($this->galleryModel);
        $form = $control->create();
        return $form;
    }



    /************************* STRANKOVANI ALB **************************/

    protected function createComponentPaginator()
    {
        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE);
        $paginator->setSnippetsNames(['albumsContainer']);

        return $paginator;
    }
}
