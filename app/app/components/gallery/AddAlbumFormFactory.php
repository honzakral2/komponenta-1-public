<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AlbumsModel,
    App\Utils;


class AddAlbumFormFactory extends Nette\Object
{
    /** @var AlbumsModel */
    private $galleryModel;

    const IMAGE_WIDTH = 190;
    const IMAGE_HEIGHT = 190;


    public function __construct(AlbumsModel $galleryModel)
    {
        $this->galleryModel = $galleryModel;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_pages');

        $form->addText('title', 'Název alba')
            ->setRequired('Zadejte prosím název vytvářeného alba.')
            ->addRule(Form::MIN_LENGTH, 'Minimální délka nazvu alba jsou %d znaky.', 2)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka nazvu alba je %d znaků.', 60);

        $form->addTextArea('description', 'Popis alba (nepovinné)', null, 3)
            ->addCondition(Form::FILLED)
                ->addRule(Form::MAX_LENGTH, 'Maximální délka popisu alba je %d znaků.', 300);

        $album_type = [
            '0' => 'Fotoalbum',
            '1' => 'Videoalbum (YT videa)'
        ];

        $form->addRadioList('album_type', 'Typ alba', $album_type)
            ->setValue(0)
            ->setRequired('Zadejte prosím typ alba.')
            ->addCondition(Form::EQUAL, 0)
                ->toggle('album-imgs');

        $form['album_type']->getSeparatorPrototype()->setName('span');

        $form->addMultiUpload('imgs', 'Zde vyberte požadované fotky (nepovinné) | minimální rozměry: ' . self::IMAGE_WIDTH . 'x' . self::IMAGE_HEIGHT)
            ->setOption('id', 'album-imgs')
            ->addCondition(Form::FILLED)
                ->addRule(Form::IMAGE, 'Obrázek musí být ve formátu JPEG, PNG nebo GIF.');

        $form->addSubmit('submit','Vytvořit album');
        $form->onSuccess[] = array($this,'addAlbumSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values array
     */
    public function addAlbumSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {

            $isVideoAlbum = $values['album_type'] == 1 ? true : false;
            $images = $values['imgs'];
            unset($values['imgs']);

            if ($isVideoAlbum) {
                $this->galleryModel->insertVideoAlbum($values);

                if ($form->hasErrors()) {
                    $form->getPresenter()->flashMessage('Album se nepodařilo vytvořit.', 'error');
                }

            } else {
                $imageDir = $this->galleryModel->getImagesDir();
                $imageAdjuster = new Utils\ImagesHandler($form, $imageDir, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
                $imageNames = $imageAdjuster->getImageNames($images);

                $this->galleryModel->insertFotoAlbum($values, $imageNames);

                if ($form->hasErrors()) {
                    $choosedImgsCount = count($images);

                    if (count($imageNames) === 0) {
                        $form->getPresenter()->flashMessage('Album bylo vytvořeno, zvolené fotky se však nepodařilo nahrát.', 'warning');
                    } elseif ($choosedImgsCount > count($imageNames)) {
                        $form->getPresenter()->flashMessage('Album bylo vytvořeno, některé fotky se však nepodařilo nahrát.', 'warning');
                    }
                }
            }

            if (!$form->hasErrors()) {
                $form->getPresenter()->flashMessage('Album bylo úspěšně vytvořeno.');
            }

            $form->getParent()->redrawControl('galleryWrapper');

        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}
