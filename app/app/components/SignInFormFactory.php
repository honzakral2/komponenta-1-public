<?php

namespace App\Components;

use Nette,
	Nette\Application\UI\Form;


class SignInFormFactory extends Nette\Object
{
	/** @var Nette\Security\User */
	private $user;


	public function __construct(Nette\Security\User $user)
	{
		$this->user = $user;
	}


    /**
     * Formular pro prihlaseni zaregistrovaneho uzivatele.
     * @return Form
     */
    public function create()
	{
		$form = new Form;
		$form->addText('username')
			->setRequired('Zadejte prosím vaše uživatelské jméno.');

		$form->addPassword('password')
			->setRequired('Zadejte prosím heslo.');

		$form->addSubmit('submit', 'Přihlásit se');
		$form->onSuccess[] = array($this, 'signInSucceeded');

		return $form;
	}

    /**
     * @param $form Form
     * @param $values
     */
    public function signInSucceeded($form, $values)
	{
        if ($form->getPresenter()->isAjax()) {

            if($this->user->isLoggedIn()) {
                $form->addError('Jste již přihlášen (jako uživatel '.$this->user->getIdentity()->username.').');
            } else {
                try {
                    $this->user->setExpiration('14 days', FALSE);
                    $this->user->login($values->username, $values->password);
                    $form->getPresenter()->flashMessage('Přihlášení proběhlo úspěšně.');
                    $form->getPresenter()->redirect('admin');

                } catch (Nette\Security\AuthenticationException $e) {
                    $form->addError($e->getMessage());
                }
            }

            if ($form->hasErrors()) {
                $form->getPresenter()->redrawControl('login');
            }

        } else {
            $form->getPresenter()->redirect('this');
        }
	}

}
