<?php
namespace App\Components;

use Nette,
    App\Model\NewsModel,
    App\Model\PageModel,
    Nette\Application\UI\Control;


class NewsControl extends Control
{
    /** @var NewsModel */
    private $newsModel;

    /** @var PageModel */
    private $pageModel;

    /** @var NewsFormFactory */
    private $newsFormFactory;

    /** @var IPaginatorControl */
    private $paginatorControl;

    private $idPages = null;
    private $idNews = null;
    private $displayNewsForm = false;

    // konstanty pro nastaveni paginatoru
    const ITEMS_PER_PAGE = 20;
    const MORE_BUTTON_TEXT = 'Další články';


    /** @var int @persistent */
    public $pageNumber = 1;


    public function __construct(
        NewsModel $newsModel,
        PageModel $pageModel,
        NewsFormFactory $newsFormFactory,
        IPaginatorControl $paginatorControl
    ) {
        parent::__construct();
        $this->newsModel = $newsModel;
        $this->pageModel = $pageModel;
        $this->newsFormFactory = $newsFormFactory;
        $this->paginatorControl = $paginatorControl;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }


    //podle typu stránky (page_type = Events/Articles) se komponenta vykreslí buď přes šablonu events.latte nebo articles.latte
    public function render()
    {
        $template = $this->template;

        $template->setFile(__DIR__ . '/templates/news.latte');

        $postsCount = $this->newsModel->getNewsCount();
        /** @var PaginatorControl $paginatorControl */
        $paginatorControl = $this['paginator'];
        $paginator = $paginatorControl->getPaginator($postsCount);

        $isAdmin = $this->getPresenter()->getView() === 'admin' ? true : false;

        $template->posts = $this->newsModel->getAllNews($paginator, $isAdmin);
        $template->displayNewsForm = $this->displayNewsForm;
        $template->idPosts = $this->idNews;

        $template->render();
    }


    /********************* INSERT, UPDATE A DELETE OPERACE *********************/

    public function handleDelete($idNews)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->newsModel->deleteNews($idNews);
            $this->redrawControl('postsWrapper');
            $this->getPresenter()->flashMessage('Zvolená novinka byla úspěšně odstraněna.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdate($idNews)
    {
        $post = $this->newsModel->getNews($idNews);

        if ($this->getPresenter()->isAjax() && $post) {
            //odstranění id ze začátku slugu. Př.: 21-article ==> article
            //$post['slug'] = preg_replace("/.*?-(.*)/", "$1", $post['slug']);
            $date = new \DateTime($post['date']);
            $post['date'] = $date->format('d.m.Y');

            $this['newsForm']->setValues($post);

            $this->idNews = $idNews;
            $this->displayNewsForm = true;
            $this->redrawControl('newsFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAdd()
    {
        if ($this->getPresenter()->isAjax()) {
            $post = array();

            $post['id_pages'] = $this->idPages;
            $date = new \DateTime();
            $post['date'] = $date->format('d.m.Y');
            $this['newsForm']->setValues($post);

            $this->displayNewsForm = true;
            $this->redrawControl('newsFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    protected function createComponentNewsForm()
    {
        return $this->newsFormFactory->create();
    }

    /************************* STRANKOVANI POSTU **************************/

    protected function createComponentPaginator()
    {
        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE);
        $paginator->setSnippetsNames(['postsContainer']);

        return $paginator;
    }

}