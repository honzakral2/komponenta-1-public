<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AdminModel,
    App\Model\IImageManager;


class PageContentFormFactory extends Nette\Object
{
	/** @var AdminModel */
	private $adminModel;

    /** @var IImageManager */
    private $imageManager;


    public function __construct(AdminModel $adminModel, IImageManager $imageManager)
	{
        $this->adminModel = $adminModel;
        $this->imageManager = $imageManager;
    }
        

    public function create()
    {
        $form = new Form;
        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_pages');

        $form->addTextArea('content')
            ->setAttribute('class', 'tinyMCE');

        $form->addSubmit('submit', 'Potvrdit změny');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        $form->onSuccess[] = array($this, 'pageSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function pageSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            $page = $this->adminModel->getPage($values['id_pages']);

            $pageId = $form->getPresenter()->getParameter('slug');
            $imagesDir = "./images/pages/$pageId";
            $imageManager = $this->imageManager->create($imagesDir, $values['content'], $page['content']);
            $values['content'] = $imageManager->getContent();

            $this->adminModel->updatePage($values, $values['id_pages']);

            $form->getParent()->redrawControl('pageContentWrapper');
            $form->getPresenter()->flashMessage('Obsah stránky byl úspěšně uložen.');

        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}
