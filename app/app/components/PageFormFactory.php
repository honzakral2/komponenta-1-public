<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\AdminModel;


class PageFormFactory extends Nette\Object
{
    /** @var AdminModel */
    private $adminModel;


    public function __construct(AdminModel $adminModel)
    {
        $this->adminModel = $adminModel;
    }


    public function createPageForm()
    {
        $form = new Form;
        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_pages');

        $form->addText('title', 'Název')
            ->setRequired('Zadejte prosím název nové sekce.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu nové sekce je %d znaků.', 45);

        $form->addText('menu_title', 'Název v menu')
            ->setRequired('Zadejte prosím název, pod kterým bude sekce dostupná z menu.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu, pod kterým bude sekce dostupná z menu, je je %d znaků.', 25);

        $form->addText('slug', 'Slug')
            ->setRequired('Zadejte prosím slug.')
            //->addRule(Form::PATTERN, 'Slug musí obsahovat pouze pomlčky, číslice a malá písmena bez diakritiky.', '(?=.*[^0-9])(?!0[1-9])(?!-)[a-z0-9-]+')
            ->addRule(Form::PATTERN, 'Slug musí obsahovat pouze pomlčky, číslice a malá písmena bez diakritiky.', '[a-z0-9-]+')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka slugu je %d znaků.', 45);

        $form->addCheckbox('homepage', 'Označit jako domovskou stránku');

        $form->addSelect('id_pages_type', 'Typ stránky')
            ->setItems($this->adminModel->getPagesType())
            ->setDefaultValue(3)
            ->setPrompt('--vyberte typ stránky--')
            ->setRequired('Vyberte prosím typ stránky.');

        $form->addSubmit('submit', 'Uložit stránku');


        $menuTitleId = $form['menu_title']->getHtmlId();
        $slugId = $form['slug']->getHtmlId();
        $form['menu_title']->setAttribute('onkeyup', 'updateSlugOnKeyUp("'.$menuTitleId.'", "'.$slugId.'")');
        $form['menu_title']->setAttribute('onblur', 'updateSlugOnKeyUp("'.$menuTitleId.'", "'.$slugId.'")');


        $form->onValidate[] = array($this, 'validatePageForm');

        $form->onSuccess[] = array($this, 'pageSucceeded');

        return $form;
    }


    /**
     * @param $form Form
     * @param $values
     */
    public function validatePageForm($form, $values)
    {
        $adminModel = $this->adminModel;

        if ($adminModel->pageTitleExists($values['title'], $values['id_pages'])) {
            $form['title']->addError('Sekce s tímto názvem již existuje.');
        }
        if ($adminModel->menuTitleExists($values['menu_title'], $values['id_pages'])) {
            $form['menu_title']->addError('Sekce s tímto názvem v menu již existuje.');
        }
        if ($adminModel->pageSlugExists($values['slug'], $values['id_pages'])) {
            $form['slug']->addError('Sekce s tímto slugem již existuje.');
        }
        if ($this->isSlugValid($values['slug'])) {
            $form['slug']->addError('Slug nesmí začínat nulou nebo pomlčkou a nesmí být složen pouze z číslic.');
        }

        if ($form->hasErrors()) {
            $form->getParent()->redrawControl('pageFormSnippet');
        }
    }

    public function isSlugValid($value)
    {
        $re = '/(?=.*[^0-9])(?!-|0)[a-z0-9-]+/';
        $result = preg_match($re, $value, $matches);

        return !$result;
    }



    /**
     * @param $form Form
     * @param $values
     */
    public function pageSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_pages'] != null) {

                $page = $this->adminModel->getPage($values['id_pages']);

                //existuje stranka, kterou chci menit?
                if ($page) {
                    $currentSlug = $page['slug'];
                    $currentPageSlug = $this->adminModel->getPageSlug($form->getPresenter()->getParameter('slug'));

                    unset($values['id_pages']);
                    $this->adminModel->updatePage($values, $page['id_pages']);

                    //pokud se nachazim na strance, kterou zrovna upravuji, a upravim jeji slug, musim provest presmerovani
                    if ($currentPageSlug == $currentSlug && $currentSlug != $values['slug']) {
                        $form->getPresenter()->redirect('this',
                            array(
                                'slug' => $values['slug'],
                                'admin-idPages' => null,
                                'admin-actionType' => null
                            )
                        );
                    } else {
                        $form->getParent()->redrawControl('pagesWrapper');
                        $form->getParent()->redrawControl('pageFormSnippet');
                        $form->getPresenter()->redrawControl('sortArea');
                    }
                } else {
                    $form->addError('Upravovaná sekce neexistuje.');
                    $form->getParent()->redrawControl('pageFormSnippet');
                }
            } else {
                $this->adminModel->addPage($values);
                //přesměrování na nově vytvořenou sekci
                $form->getPresenter()->flashMessage('Vytvoření nové sekce proběhlo úspešně.');
                $form->getPresenter()->redirect('admin', array('slug' => $values['slug']));
            }
        } else {
            $form->getPresenter()->redirect('this');
        }
    }

}
