<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\CommentsModel;


class CommentFormFactory extends Nette\Object
{
    /** @var CommentsModel */
    private $commentsModel;

    /** @var Nette\Security\User */
    private $user;


    public function __construct(CommentsModel $commentsModel, Nette\Security\User $user)
    {
        $this->commentsModel = $commentsModel;
        $this->user = $user;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";
        $form->getElementPrototype()->class[] = "comment-form";

        // antispam
        $time = new \DateTime();
        $form->addHidden('datetime', $time->format('d. m. Y H:i:s'));
        $form->addHidden('email');
        $form->addHidden('url');

        $form->addHidden('id_items')
            ->addCondition(Form::FILLED)
            ->addRule(Form::NUMERIC);

        $form->addHidden('reply_on_id_comments')
            ->addCondition(Form::FILLED)
            ->addRule(Form::NUMERIC);

        $form->addHidden('nesting_in_id_comments')
            ->addCondition(Form::FILLED)
            ->addRule(Form::NUMERIC);

        $form->addTextArea('text'/*, 'Text'*/)
            ->setAttribute('placeholder','Text komentáře')
            ->setRequired('Zadejte prosím text komentáře.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka komentáře je %d znaků.', 500);

        if (!$this->user->isLoggedIn()) {
            $form->addText('author'/*, 'Jméno'*/)
                ->setAttribute('placeholder','Vaše jméno/přezdívka')
                ->setRequired('Zadejte prosím jméno, pod kterým chcete v rámci komentáře vystupovat.')
                ->addRule(Form::MIN_LENGTH, 'Minimální délka jména jsou %d znaky.', 2)
                ->addRule(Form::MAX_LENGTH, 'Maximální délka jména je %d znaků.', 45);
        }

        $form->addSubmit('submit', 'Vložit');

        if ($this->user->isLoggedIn())
            $form['submit']->setAttribute('class','admin');


        $form->onSuccess[] = array($this, 'commentSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function commentSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {

            $this->antiSpam($form, $values);

            // v pripade, ze nejde o reakci na jiny komentar
            if (!$values['reply_on_id_comments'] || !$values['nesting_in_id_comments']) {
                $values['reply_on_id_comments'] = NULL;
                $values['nesting_in_id_comments'] = NULL;

            } else { // pokud jde o reakci
                $commentsExists = $this->commentsModel->commentExists($values['reply_on_id_comments']) &&
                    $this->commentsModel->commentExists($values['nesting_in_id_comments']);

                if (!$commentsExists)
                    $form->addError('Komentář, na který chcete reagovat, již neexistuje.');
            }

            if (!$form->hasErrors()) {

                if ($this->user->isLoggedIn()) {
                    $values['id_users'] = $this->user->getId();
                    unset($values['author']);

                    if ($this->user->isInRole('admin')) {
                        $values['author_is_admin'] = true;
                    }
                }

                $values['date'] = new \DateTime();

                $this->commentsModel->addComment($values);

                // formular se vyprazdni a aktualizuje se datetime (ne date!)
                $datetime = new \DateTime();
                $form->setValues([
                    'datetime' => $datetime->format('d. m. Y H:i:s')
                ], TRUE);
            }

            $form->getParent()->redrawControl('commentsWrapper');

        } else {
            $form->getPresenter()->redirect('this');
        }
    }


    /**
     * Pokud jsou vyplnena pole, ktera jsou hidden a cas odeslani je nizsi nebo roven trem vterinam,
     * predpoklada se, ze formular odeslal robot => prispevek se nevlozi a dojde k presmerovani.
     *
     * @param $form Form
     * @param $values
     */
    private function antiSpam($form, $values)
    {
        $timeSend = new \DateTime('-3 second');
        $datetime = date_create_from_format('d. m. Y H:i:s', $values['datetime']);

        if ($values['email'] || $values['url'] || $datetime >= $timeSend) {
            $form->getPresenter()->redirect('this');
        } else {
            unset($values['datetime']);
            unset($values['email']);
            unset($values['url']);
        }
    }

}
