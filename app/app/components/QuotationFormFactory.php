<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\QuotationsModel,
    App\Utils\ImageHandler;


class QuotationFormFactory extends Nette\Object
{
    /** @var QuotationsModel */
    private $quotationModel;

    const IMAGE_WIDTH = 90;
    const IMAGE_HEIGHT = 90;


    public function __construct(QuotationsModel $quotationModel)
    {
        $this->quotationModel = $quotationModel;
    }


    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";
        
        $form->addHidden('id_pages');
        $form->addHidden('id_quotations');
        $form->addText('quotation', 'Citace')
            ->setRequired('Zadejte prosím citaci.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu je %d znaků.', 1000);
        $form->addText('details', 'Podrobnosti')
            ->setRequired('Zadejte prosím podrobnosti.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu je %d znaků.', 255);
        $form->addUpload(QuotationsModel::IMAGE_COLUMN_NAME, 'Fotka (nepovinné) | minimální rozměry: ' . self::IMAGE_WIDTH . 'x' . self::IMAGE_HEIGHT)
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, 'Fotka musí být JPG, PNG nebo GIF.');
        $form->addSelect('priority', 'Priorita citace')
                ->setRequired('Priorita citace je povinná')
                ->setItems(['1' => '1 (nejvyšší)',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5 (nejnižší)'])
                ->setPrompt('--vyberte prioritu--');
        
        $form->addSubmit('submit', 'Uložit citaci');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        $form->onSuccess[] = array($this, 'quotationSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function quotationSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_quotations'] != null) {

                $quotation = $this->quotationModel->getQuotation($values['id_quotations']);

                //existuje citace, kterou chci zmenit?
                if ($quotation) {
                    $values['title'] = substr($values['quotation'], 0, 50);
                    unset($values['id_pages']);

                    $this->createThumbnail($form, $values, $values['id_quotations']);

                    $this->quotationModel->updateQuotation($values, $values['id_quotations']);

                    $form->getParent()->redrawControl('quotationsList');
                    $form->getPresenter()->flashMessage('Citace byla úspěšně upravena.');
                }
            } else {
                $values['title'] = substr($values['quotation'], 0, 50);

                $this->createThumbnail($form, $values, $values['id_quotations']);

                $this->quotationModel->addQuotation($values);

                $form->getParent()->redrawControl('quotationsWrapper');
                $form->getPresenter()->flashMessage('Citace byla úspěšně přidána.');
            }
            $form->getParent()->redrawControl('quotationFormSnippet');
        } else {
            $form->getPresenter()->redirect('this');
        }
    }

    private function createThumbnail($form, $values, $id)
    {
        if ($values[QuotationsModel::IMAGE_COLUMN_NAME]->getError() != 4) {
            $imageDir = $this->quotationModel->getImagesDir($id);
            $imageHandler = new ImageHandler($form, $imageDir, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);

            $values[QuotationsModel::IMAGE_COLUMN_NAME] =
                $imageHandler->getImageName($values[QuotationsModel::IMAGE_COLUMN_NAME]);

        } elseif ($id) {
            unset($values[QuotationsModel::IMAGE_COLUMN_NAME]);
        }
    }
}
