<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Control;


class PaginatorControl extends Control
{
    /** @var array */
    private $snippetsNames;

    /** @var string */
    private $moreButtonText;

    /** @var int */
    private $itemsPerPage;

    /** @var Nette\Utils\Paginator */
    private $paginator;

    /** @var int @persistent */
    public $pageNumber = 1;


    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/paginator.latte');

        $template->paginator = $this->paginator;
        $template->moreButtonText = $this->moreButtonText;

        $template->render();
    }

    /**
     * @param $buttonText string
     */
    public function setButtonText($buttonText)
    {
        $this->moreButtonText = $buttonText;
    }

    /**
     * @param $snippetsNames array
     */
    public function setSnippetsNames($snippetsNames)
    {
        $this->snippetsNames = $snippetsNames;
    }

    /**
     * @param int $itemsPerPage
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @param $concertsCount int
     * @return Nette\Utils\Paginator
     */
    public function getPaginator($concertsCount)
    {
        $this->paginator = new Nette\Utils\Paginator;
        $this->paginator->setItemCount($concertsCount);
        $this->paginator->setItemsPerPage($this->itemsPerPage);
        $this->paginator->setPage($this->pageNumber);
        return $this->paginator;
    }

    /**
     * Nacitani dalsich polozek.
     */
    public function handleMore()
    {
        if ($this->getPresenter()->isAjax()) {

            $this->pageNumber >= 2
                ? $this->pageNumber++
                : $this->pageNumber = 2;

            foreach ($this->snippetsNames as $snippet) {
                $this->getParent()->redrawControl($snippet);
            }
            $this->redrawControl('paginator');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }
}
