<?php

namespace App\Components;

use Nette,
	Nette\Application\UI\Form,
	\App\Model\AdminModel;

class UserManagerFormFactory extends Nette\Object
{
	/** @var AdminModel */
	private $adminModel;


	public function __construct(AdminModel $adminModel)
	{
		$this->adminModel = $adminModel;
	}


    /**
     * Formular pro registraci noveho uzivatele.
     * @return Form
     */
    public function createAddUserForm()
	{
        /** @var Form $form */
		$form = new Form;

        $form->getElementPrototype()->class[] = "ajax";
        //$form->getElementPrototype()->class('ajax');
        //$form->getElementPrototype()->addAttributes(array("class" => "ajax"));

        $form->addText('username','Uživatelské jméno')
            ->setRequired('Zadejte prosím uživatelské jméno.')
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, 'Minimální délka uživatelského jména jsou %d znaky.', 2)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka uživatelského jména je %d znaků.', 15)
            ->addRule(Form::PATTERN, 'Zadané jméno obsahuje nepovolené znaky', '^[a-zA-Z0-9áčďéěíňóřšťůúýžÁČĎÉĚÍŇÓŘŠŤŮÚÝŽ]+$')
            ->addRule(function ($control) {
                return $this->isUsernameAvailable($control->value);
            }, 'Uživatel s tímto uživatelským jménem již existuje.');

        $form = $this->userDataInputs($form);

        $form->addPassword('password','Heslo')
            ->addRule(Form::FILLED, 'Vyplňte prosím heslo.')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6);

        $form->addPassword('passwordCheck','Heslo znova')
            ->setRequired('Zadejte prosím heslo znova - pro kontrolu.')
            ->addConditionOn($form['password'], Form::VALID)
            ->addRule(Form::FILLED, 'Zadejte prosím heslo znova - pro kontrolu.')
            ->addRule(Form::EQUAL, 'Hesla se neshodují.', $form['password']);

        $form->addSubmit('submit','Přidat uživatele');

        $form->onValidate[] = array($this, 'validateManageUserForm');

		$form->onSuccess[] = array($this,'manageUserSucceeded');

		return $form;
	}

    /**
     * @param $form Form
     * @param $values
     */
    public function validateManageUserForm($form)
    {
        if ($form['username']->error)
            $form->getParent()->redrawControl('addUserForm');
    }


    /**
     * Spolecna cast pro createAddUserForm a createUpdateUserForm,
     * obshujici inputy pro jmeno a prijmeni uzivatele.
     * @param $form Form
     * @return Form
     */
    private function userDataInputs($form)
    {
        $form->addText('name','Jméno (pod kterým uživatel na webu vystupuje)')
            ->addCondition(Form::FILLED)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka uživatelského jména je %d znaků.', 45);

        /*$form->addText('surname','Příjmení:')
            ->addCondition(Form::FILLED)
            ->addRule(Form::MAX_LENGTH, 'Maximální délka uživatelského jména je %d znaků.', 45);*/

        return $form;
    }

    /**
     * Vraci, jestli je k dispozici zadane uzivatelske jmeno.
     * Tuto metodu vyuziva pravidlo v createSignUpForm.
     * @param $value
     * @return bool
     */
    public function isUsernameAvailable($value)
    {
        return !$this->adminModel->usernameExists($value);
    }

    /**
     * Formular pro upravu jmena a prijmeni zvoleneho uzivatele.
     * @return Form
     */
    public function createUpdateUserForm()
    {
        $form = new Form;

        $form->getElementPrototype()->class('ajax');

        $form->addHidden('id_users');

        $form = $this->userDataInputs($form);

        $form->addSubmit('submit','Uložit změnu');

        $form->onSuccess[] = array($this,'manageUserSucceeded');

        return $form;
    }

    /**
     * Formular pro zmenu hesla zvoleneho uzivatele.
     * @return Form
     */
    public function createUpdatePasswordForm()
    {
        $form = new Form;

        $form->getElementPrototype()->class('ajax');

        $form->addHidden('id_users');

        $form->addPassword('password','Nové heslo:')
            ->addRule(Form::FILLED, 'Zadejte nové heslo')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6);

        $form->addPassword('passwordCheck','Heslo znova:')
            ->setRequired('Zadejte prosím heslo znova - pro kontrolu.')
            ->addConditionOn($form['password'], Form::VALID)
            ->addRule(Form::FILLED, 'Zadejte prosím heslo znova - pro kontrolu.')
            ->addRule(Form::EQUAL, 'Hesla se neshodují.', $form['password']);

        $form->addSubmit('submit','Uložit změnu');

        $form->onSuccess[] = array($this,'manageUserSucceeded');

        return $form;
    }

    /**
     * Zpracovani formulare (jakehokoli z vyse definovanych) po odeslani.
     * @param $form Form
     * @param $values
     */
    public function manageUserSucceeded($form, $values)
	{
        if ($form->getPresenter()->isAjax()) {

            if (!isset($values['id_users'])) {
                try {
                    $this->adminModel->addUser($values);
                } catch (Nette\Database\UniqueConstraintViolationException $e) {
                    $form->addError($e->getMessage());
                }

                $flashMessage = 'Vytvoření uživatelského účtu proběhlo úspěšně !';
                $snippetName = 'addUserForm';

            } else {
                if (isset($values['password'])) {
                    $this->adminModel->updatePassword($values, $values['id_users']);

                    $flashMessage = 'Změna hesla proběhla úspěšně !';
                    $snippetName = 'updatePasswordForm';
                } else {
                    $this->adminModel->updateUser($values, $values['id_users']);

                    $flashMessage = 'Úprava údajů o uživateli proběhla úspěšně !';
                    $snippetName = 'updateUserForm';
                }
            }

            if ( !$form->hasErrors() ) {
                $form->setValues(array(), TRUE);
                $form->getPresenter()->flashMessage($flashMessage);
                $form->getParent()->redrawControl('usersList');
            }

            $form->getParent()->redrawControl($snippetName);

        } else {
            $form->getPresenter()->redirect('this');
        }
	}
}
