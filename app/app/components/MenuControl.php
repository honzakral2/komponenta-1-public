<?php

namespace App\Components;

use Nette,
    App\Model\RouterCacheManager,
    App\Model\MenuModel,
    Nette\Application\UI\Control;

class MenuControl extends Control
{
    /** @var MenuModel */
    private $menuModel;

    /** @var RouterCacheManager */
    private $routerCacheManager;

    /** @var Nette\Http\SessionSection */
    private $sessionSection;


    public function __construct(MenuModel $menuModel, RouterCacheManager $routerCacheManager, Nette\Http\Session $session)
    {
        parent::__construct();
        $this->menuModel = $menuModel;
        $this->routerCacheManager = $routerCacheManager;
        $this->sessionSection = $session->getSection('newNews');
    }


    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/menu.latte');

        $template->menu = $this->menuModel->getMenu();
        $template->pageId = $this->getPresenter()->getParameter('slug');
        $template->activePageId = $this->getActivePageId();
        $template->homepageId = $this->routerCacheManager->getHomepageId();

        $template->newNewsCount = $this->getNewNewsCount();

        if ($template->activePageId == $template->homepageId ||
            $template->activePageId == 9) {
            $slides = $this->menuModel->getSlides();
            $template->slides = $slides;
            $template->slidesCount = count($slides);
        }

        $template->render();
    }


    private function getNewNewsCount()
    {
        /* Tato cast kodu umoznuje mazani pole s navstivenymi novinkami.
         * Jinymi slovy zajisti, aby byly v tomto poli pouze aktualni novinky (stare max tyden).
         */
        if (!isset($this->sessionSection['visitDate']))
            $this->sessionSection['visitDate'] = new \DateTime();

        $weekAgo = new \DateTime("-1 week");
        if ($this->sessionSection['visitDate'] < $weekAgo) {
            unset($this->sessionSection['newNewsVisited']);
            $this->sessionSection['visitDate'] = new \DateTime();
        }

        // nacte pole novinek pridanych za posledni tyden
        $latestNews = $this->menuModel->getLatestNews();

        // pokud je uzivatel na strance s novinkami, vsechny se oznaci jako prectene
        $pageId = $this->getPresenter()->getParameter('slug');
        if ($pageId === 4) {
            $this->sessionSection['newNewsVisited'] = $latestNews;
        }

        // pokud je uzivatel na strance s konkretni novinkou, je tato oznacena jako prectena
        $itemType = $this->getPresenter()->getParameter('itemType');
        if ($itemType === 'news') {
            $slugId = $this->getPresenter()->getParameter('itemSlug');
            $newNewsVisited = $this->sessionSection['newNewsVisited'];

            if (isset($latestNews[$slugId]) && !isset($newNewsVisited[$slugId])) {
                $newNewsVisited[$slugId] = $latestNews[$slugId];
                $this->sessionSection['newNewsVisited'] = $newNewsVisited;
            }
        }

        // vypocet poctu neprectenych/novych novinek
        $newsVisitedCount = count($this->sessionSection['newNewsVisited']);
        $newNewsCount = count($latestNews);

        // k tomuto muze dojit, pokud je nektera novinka smazana
        if ($newsVisitedCount > $newNewsCount) {
            $this->sessionSection['newNewsVisited'] = array_intersect_assoc($latestNews, $this->sessionSection['newNewsVisited']);
            $newsVisitedCount = $newNewsCount;
        }

        if ($newsVisitedCount > 0 && $newNewsCount > 0) {
            $this->sessionSection['newNewsCount'] = $newNewsCount - $newsVisitedCount;
        } else {
            $this->sessionSection['newNewsCount'] = $newNewsCount;
        }

        return $this->sessionSection['newNewsCount'] <= 3 ? $this->sessionSection['newNewsCount'] : 0;
    }


    private function getActivePageId()
    {
        $activePageId = -1;
        $presenterName = $this->getPresenter()->getName();

        if ($presenterName === 'Post') {
            $slugId = $this->getPresenter()->getParameter('itemSlug');
            $activePageId = $this->menuModel->getActivePageId($slugId);
        }

        if ($presenterName === 'Page') {
            $activePageId = $this->getPresenter()->getParameter('slug');
        }

        return $activePageId;
    }

}
