<?php

namespace App\Components;

use Nette,
    App\Model\AlbumsModel,
    Nette\Application\UI\Control,
    App\Model\PageModel,
    App\Model\ConcertsModel;

class HomepageControl extends Control
{
    /** @var AlbumsModel */
    private $galleryModel;

    /** @var ConcertsModel */
    private $concertModel;

    /** @var PageModel */
    private $pageModel;

    /** @var PageContentFormFactory */
    private $pageContentFormFactory;

    /** @var IPaginatorControl */
    private $paginatorControl;


    private $idPages = -1;


    public function __construct(
        AlbumsModel $galleryModel,
        ConcertsModel $concertModel,
        PageModel $pageModel,
        PageContentFormFactory $pageContentFormFactory,
        IPaginatorControl $paginatorControl
    ) {
        parent::__construct();

        $this->galleryModel = $galleryModel;
        $this->concertModel = $concertModel;
        $this->pageModel = $pageModel;
        $this->pageContentFormFactory = $pageContentFormFactory;
        $this->paginatorControl = $paginatorControl;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }

    public function render($pageType = '')
    {
        $template = $this->template;

        $template->addFilter('month','Helpers::czechMonth');
        $template->setFile(__DIR__ . '/templates/homepage.latte');

        $template->pageType = $pageType;

        if ($pageType === 'en') {
            $page = $this->pageModel->getPageContent($this->idPages);
            $this->template->page = $page;

            if ($this->getPresenter()->getView() === 'admin') {
                $page['id_pages'] = $this->idPages;
                $this['pageContentForm']->setValues($page);
            }
        }

        $template->concerts = $this->concertModel->getFutureConcerts(5);
        //$template->youtubeVideo = $this->getYoutubeVideo();

        $template->images = $this->galleryModel->getLastImages();

        $template->render();
    }


    protected function createComponentPageContentForm()
    {
        return $this->pageContentFormFactory->create();
    }
}
