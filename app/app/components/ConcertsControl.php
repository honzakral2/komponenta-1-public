<?php
namespace App\Components;

use Nette,
    App\Model\ConcertsModel,
    Nette\Application\UI\Control;


class ConcertsControl extends Control
{
    /** @var ConcertsModel */
    private $concertsModel;

    /** @var ConcertFormFactory */
    private $concertFormFactory;

    /** @var PaginatorControl */
    private $paginatorControl;

    private $idPages = null;
    private $idConcerts = null;
    private $displayConcertForm = false;

    // konstanty pro nastaveni paginatoru
    const ITEMS_PER_PAGE = 5;
    const MORE_BUTTON_TEXT = 'Další koncerty';


    public function __construct(
        ConcertsModel $concertsModel,
        ConcertFormFactory $concertFormFactory,
        IPaginatorControl $paginatorControl
    ) {
        parent::__construct();
        $this->concertsModel = $concertsModel;
        $this->concertFormFactory = $concertFormFactory;
        $this->paginatorControl = $paginatorControl;
    }

    protected function attached($presenter)
    {
        parent::attached($presenter);

        if (!$presenter instanceof Nette\Application\UI\Presenter)
            return;

        $this->idPages = $presenter->getParameter('slug');
    }

    public function render()
    {
        $template = $this->template;

        $template->addFilter('month','Helpers::czechMonth');

        $template->setFile(__DIR__ . '/templates/concerts.latte');

        $concertsCount = $this->concertsModel->getPastConcertsCount();

        /** @var PaginatorControl $paginatorControl */
        $paginatorControl = $this['paginator'];
        $paginator = $paginatorControl->getPaginator($concertsCount);

        $template->futureConcerts = $this->concertsModel->getFutureConcerts();
        $template->pastConcerts = $this->concertsModel->getPastConcerts($paginator);
        $template->displayConcertForm = $this->displayConcertForm;
        $template->idConcerts = $this->idConcerts;

        $template->render();
    }


    /********************* INSERT, UPDATE A DELETE OPERACE *********************/

    public function handleDelete($idConcerts)
    {
        if ($this->getPresenter()->isAjax()) {
            $this->concertsModel->deleteConcert($idConcerts);
            $this->redrawControl('concertsWrapper');
            $this->getPresenter()->flashMessage('Zvolený koncert byl úspěšně odstraněn.');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleUpdate($idConcerts)
    {
        $concert = $this->concertsModel->getConcert($idConcerts);

        if ($this->getPresenter()->isAjax() && $concert) {
            $date = new Nette\Utils\DateTime($concert['date']);
            $concert['time'] = $date->format('H:i');
            $concert['date'] = $date->format('d.m.Y');
            $this['concertForm']->setValues($concert);

            $this->idConcerts = $idConcerts;
            $this->displayConcertForm = true;
            $this->redrawControl('concertFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }

    public function handleAdd()
    {
        if ($this->getPresenter()->isAjax()) {
            $this->displayConcertForm = true;

            $concert['id_pages'] = $this->idPages;
            $this['concertForm']->setValues($concert);

            $this->redrawControl('concertFormSnippet');
        } else {
            $this->getPresenter()->redirect('this');
        }
    }
    
    protected function createComponentConcertForm()
    {
        return $this->concertFormFactory->create();
    }

    protected function createComponentPaginator()
    {
        $paginator = $this->paginatorControl->create();
        $paginator->setButtonText(self::MORE_BUTTON_TEXT);
        $paginator->setItemsPerPage(self::ITEMS_PER_PAGE);
        $paginator->setSnippetsNames(['concertsContainer']);

        return $paginator;
    }
}