<?php

namespace App\Components;

use Nette;


interface IPaginatorControl
{
    /** @return PaginatorControl */
    function create();
}
