<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\NewsModel,
    App\Model\IImageManager,
    App\Utils\ImageHandler;


class NewsFormFactory extends Nette\Object
{
    /** @var NewsModel */
    private $newsModel;

    /** @var IImageManager */
    private $imageManager;

    const POST_SLUG_ID = 'frm-postSlug';

    const IMAGE_WIDTH = 330;
    const IMAGE_HEIGHT = 230;


    public function __construct(NewsModel $newsModel, IImageManager $imageManager)
    {
        $this->newsModel = $newsModel;
        $this->imageManager = $imageManager;
    }


    //doplnit dynamicke plneni authora, idPages
    public function create()
    {
        $form = new Form;
        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_news');
        $form->addHidden('id_pages');
        $form->addText('title', 'Název')
            ->setRequired('Zadejte prosím název.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu je %d znaků.', 255);
        /*$form->addText('slug', 'Slug')
            ->setRequired('Zadejte prosím slug.')
            ->addRule(Form::PATTERN, 'Slug musí obsahovat pouze pomlčky, číslice a malá písmena bez diakritiky.', '[a-z0-9-]+')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka slugu je %d znaků.', 255);*/
        //$form->addTextArea('perex', 'Perex');

        $form->addText('date', 'Datum zveřejnění novinky')
            ->setRequired('Zadejte prosím den, kdy se má novinka zobrazit.')
            ->setAttribute('placeholder', "dd.mm.rrrr")
            ->addRule(Form::PATTERN, 'Datum musí být ve tvaru dd.mm.rrrr, tedy např. 31.12.1992', "\d\d?\.?\d\d?\.?\d{4}");

        $form->addUpload(NewsModel::IMAGE_COLUMN_NAME, 'Úvodní fotka (nepovinné) | minimální rozměry: ' . self::IMAGE_WIDTH . 'x' . self::IMAGE_HEIGHT)
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, 'Fotka musí být JPG, PNG nebo GIF.');

        $form->addTextArea('content', 'Text')
            ->setRequired('Zadejte prosím text článku.')
            ->setAttribute('class', ' tinyMCE');

        /*$form->addCheckbox('comments_allowed','Povolit komentáře?')
            ->setValue(1);*/

        $form->addCheckbox('in_slideshow','Chcete tento příspěvek zobrazit ve slideshow?')
            ->setValue(0);

        $form->addSubmit('submit', 'Uložit příspěvek');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        /*$titleId = $form['title']->getHtmlId();
        $form['slug']->setHtmlId(self::POST_SLUG_ID);
        $form['title']->setAttribute('onkeyup', 'updateSlugOnKeyUp("'.$titleId.'", "'.self::POST_SLUG_ID.'")');
        $form['title']->setAttribute('onblur', 'updateSlugOnKeyUp("'.$titleId.'", "'.self::POST_SLUG_ID.'")');*/

        $form->onSuccess[] = array($this, 'postSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function postSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_news'] != null) {

                $post = $this->newsModel->getNews($values['id_news']);

                //existuje prispevek, ktery chci zmenit?
                if ($post) {
                    $values['date'] = new \DateTime($values['date']);
                    unset($values['id_pages']);
                    /*$values['slug'] = '0'.$values['id_news'].'-'.$values['slug'];
                    $currentSlug = $post['slug'];
                    $currentPostSlug = $this->postModel->getPostSlug($form->getPresenter()->getParameter('postSlug'));*/

                    $this->createThumbnail($form, $values, $values['id_news']);

                    $imageDir = $this->newsModel->getImagesDir($values['id_news']);
                    $imageManager = $this->imageManager->create($imageDir, $values['content'], $post['content']);
                    $values['content'] = $imageManager->getContent();
                    $values['perex'] = $this->createPerex($values['content']);

                    $this->newsModel->updateNews($values, $values['id_news']);

                    //pokud se nachazim na strance (prispevku), ktery zrovna upravuji, a upravim jeho slug, musim provest presmerovani
                    /*if ($currentPostSlug == $currentSlug && $currentSlug != $values['slug']) {
                        $form->getPresenter()->redirect('this', array('postSlug' => $values['slug']));
                    } else {*/

                        $form->getParent()->redrawControl('postsList');
                        $form->getParent()->redrawControl('postWrapper');
                        $form->getPresenter()->redrawControl('commentsControl');
                        $form->getPresenter()->flashMessage('Novinka byla úspěšně upravena.');
                    //}
                }
            } else {
                $values['comments_allowed'] = true;
                $values['date'] = new \DateTime($values['date']);
                //$values['slug'] = '0'.$idNews.'-'.$values['slug'];

                $this->createThumbnail($form, $values, $values['id_news']);

                $imageDir = $this->newsModel->getImagesDir($values['id_news']);
                $imageManager = $this->imageManager->create($imageDir, $values['content'], '');
                $values['content'] = $imageManager->getContent();
                $values['perex'] = $this->createPerex($values['content']);

                $this->newsModel->addNews($values);

                $form->getParent()->redrawControl('postsWrapper');
                $form->getPresenter()->flashMessage('Novinka byla úspěšně přidána.');
            }

            $form->getParent()->redrawControl('newsFormSnippet');

        } else {
            $form->getPresenter()->redirect('this');
        }
    }


    private function createThumbnail($form, $values, $id)
    {
        if ($values[NewsModel::IMAGE_COLUMN_NAME]->getError() != 4) {
            $imageDir = $this->newsModel->getImagesDir($id);
            $imageHandler = new ImageHandler($form, $imageDir, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);

            $values[NewsModel::IMAGE_COLUMN_NAME] =
                $imageHandler->getImageName($values[NewsModel::IMAGE_COLUMN_NAME]);

        } elseif ($id) {
            unset($values[NewsModel::IMAGE_COLUMN_NAME]);
        }
    }

    //vytvoří perex ze zadaného textu - zkrátí text na vhodný počet znaků + odstraní z něj HTML tagy
    private function createPerex($text)
    {
        // Odstraní i text odkazu pokud je ve tvaru www. nebo http(s)://
        $text = preg_replace('/<a\b[^>]*>(www.|http:\/\/|https:\/\/)(.*?)<\/a>/i', '', $text);
        // Odstraní html tagy a zkrátí text na 500 znaků
        $text = substr(strip_tags($text), 0, 500);
        // Odkazem bývá často dvojtečka => změní ji na tečku, protože tam ten odkaz už není
        $text = preg_replace('/(:)[^0-9]/i', '. ', $text);
        // Při odstranění tagů s odstavci a zalamování řádků zanikne mezera mezi bloky textu => přidá ji
        $perex = preg_replace('/(\.|!|\?)([A-Z])/', '$1 $2', $text);
        return $perex;
    }
}
