<?php

namespace App\Components;

use Nette,
    Nette\Application\UI\Form,
    App\Model\ReferencesModel,
    App\Model\IImageManager;


class ReferenceFormFactory extends Nette\Object
{
    /** @var ReferencesModel */
    private $referenceModel;

    /** @var IImageManager */
    private $imageManager;


    public function __construct(ReferencesModel $referenceModel, IImageManager $imageManager)
    {
        $this->referenceModel = $referenceModel;
        $this->imageManager = $imageManager;
    }


    //public function createReferenceForm()
    public function create()
    {
        $form = new Form;

        $form->getElementPrototype()->class[] = "ajax";

        $form->addHidden('id_pages');
        $form->addHidden('id_references');
        $form->addText('title', 'Název')
            ->setRequired('Zadejte prosím název.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka názvu je %d znaků.', 255);
        //$form->addHidden('slug');
        $form->addText('date', 'Datum')
            ->setRequired('Zadejte prosím datum.')
            ->setAttribute('placeholder', "dd.mm.rrrr")
            ->addRule(Form::PATTERN, 'Datum musí být ve tvaru dd.mm.rrrr, tedy např. 31.12.1992', "\d\d?\.?\d\d?\.?\d{4}");
        $form->addText('source', 'Médium')
            ->setRequired('Zadejte prosím médium.')
            ->addRule(Form::MAX_LENGTH, 'Maximální délka místa je %d znaků.', 255);
        $form->addTextArea('content', 'Obsah')
            ->setRequired('Zadejte prosím obsah článku.')
            ->setAttribute('class', 'tinyMCE');
        
        $form->addSubmit('submit', 'Uložit článek');

        $form->getComponent('submit')->getControlPrototype()->onclick('tinyMCE.triggerSave()');

        $form->onSuccess[] = array($this, 'referenceSucceeded');

        return $form;
    }

    /**
     * @param $form Form
     * @param $values
     */
    public function referenceSucceeded($form, $values)
    {
        if ($form->getPresenter()->isAjax()) {
            if ($values['id_references'] != null) {

                $reference = $this->referenceModel->getReference($values['id_references']);

                //existuje clanek, ktery chci zmenit?
                if ($reference) {
                    $values['date'] = new \DateTime($values['date']);
                    unset($values['id_pages']);
                    /*$values['slug'] = '0'.$values['id_references'].'-'.substr(Nette\Utils\Strings::webalize($values['title']), 0, 50);
                    $currentSlug = $reference['slug'];
                    $currentPostSlug = $this->referenceModel->getReferenceSlug($form->getPresenter()->getParameter('postSlug'));*/

                    $imageDir = $this->referenceModel->getImagesDir($values['id_references']);
                    $imageManager = $this->imageManager->create($imageDir, $values['content'], $reference['content']);
                    $values['content'] = $imageManager->getContent();
                    $values['perex'] = $this->createPerex($values['content']);
                    
                    $this->referenceModel->updateReference($values, $values['id_references']);

                    //pokud se nachazim na strance (prispevku), ktery zrovna upravuji, a upravim jeho slug, musim provest presmerovani
                    /*if ($currentPostSlug == $currentSlug && $currentSlug != $values['slug']) {
                        $form->getPresenter()->redirect('this', array('postSlug' => $values['slug']));
                    } else {*/
                        $form->getParent()->redrawControl('referencesList');
                        $form->getParent()->redrawControl('referenceWrapper');
                        $form->getPresenter()->redrawControl('commentsControl');

                        $form->getPresenter()->flashMessage('Článek byl úspěšně upraven.');
                    //}
                }

            } else {
                $values['comments_allowed'] = true;
                $values['date'] = new \DateTime($values['date']);

                $imageDir = $this->referenceModel->getImagesDir($values['id_references']);
                $imageManager = $this->imageManager->create($imageDir, $values['content'], '');
                $values['content'] = $imageManager->getContent();
                $values['perex'] = $this->createPerex($values['content']);

                $this->referenceModel->addReference($values);

                $form->getParent()->redrawControl('referencesWrapper');
                $form->getPresenter()->flashMessage('Článek byl úspěšně přidán.');
            }
            $form->getParent()->redrawControl('referenceFormSnippet');
        } else {
            $form->getPresenter()->redirect('this');
        }
    }
    
    //vytvoří perex ze zadaného textu - zkrátí text na vhodný počet znaků + odstraní z něj HTML tagy
    public function createPerex($text) {
        // Odstraní i text odkazu pokud je ve tvaru www. nebo http(s)://
        $text = preg_replace('/<a\b[^>]*>(www.|http:\/\/|https:\/\/)(.*?)<\/a>/i', '', $text);
        // Odstraní html tagy a zkrátí text na 500 znaků
        $text = substr(strip_tags($text), 0, 500);
        // Odkazem bývá často dvojtečka => změní ji na tečku, protože tam ten odkaz už není
        $text = preg_replace('/(:)[^0-9]/i', '. ', $text);
        // Při odstranění tagů s odstavci a zalamování řádků zanikne mezera mezi bloky textu => přidá ji
        $perex = preg_replace('/(\.|!|\?)([A-Z])/', '$1 $2', $text);
        return $perex;
    }
}
