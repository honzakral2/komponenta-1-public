<?php

namespace App;

use Nette,
	Nette\Security\Permission;

class Authorizator extends Nette\Object implements Nette\Security\IAuthorizator
{
	/** @var Nette\Security\Permission */
    private $acl;

    public function __construct()
    {
        $this->acl = new Permission();


		/********************* ROLE *********************/
		
        $this->acl->addRole('guest');
        $this->acl->addRole('editor','guest');
        $this->acl->addRole('admin');
				
		
		/******************** ZDROJE ********************/ 

        $this->acl->addResource('Base');

        $this->acl->addResource('Page','Base');
        $this->acl->addResource('Error','Base');
        $this->acl->addResource('Article','Base');

        $this->acl->addResource('Articles','Page');
        $this->acl->addResource('Events','Page');

        $this->acl->addResource('Post','Base');
        $this->acl->addResource('Album','Base');

		/******************** PRAVA *********************/
		
		// práva přidělená nepřihlášenému uživateli
        $this->acl->allow('guest','Base');
        $this->acl->deny('guest','Base',array('admin'));

        // práva přidělená editorovi
        $this->acl->allow('editor','Base',array('admin'));

		// práva admin může dělat cokoliv a kdekoli
        $this->acl->allow('admin');
    }


    function isAllowed($role, $resource, $privilege)
    {
        return $this->acl->isAllowed($role, $resource, $privilege);
    }
}