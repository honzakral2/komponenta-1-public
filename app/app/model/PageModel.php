<?php

namespace App\Model;

use Nette;


class PageModel extends Nette\Object
{
	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

    public function getPageContent($id)
    {
        return $this->database->query('
		SELECT title, content, homepage
		FROM pages
		WHERE id_pages = ?',$id)->fetch();
    }


	public function getPageType($id) {
		return $this->database->query('
		SELECT pages_type.name
		FROM pages
		JOIN pages_type
			USING (id_pages_type)
		WHERE id_pages = ?',$id)->fetchField(0);
	}
}
