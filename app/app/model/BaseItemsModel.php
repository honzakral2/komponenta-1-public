<?php

namespace App\Model;

use Nette;


abstract class BaseItemsModel extends Nette\Object
{
	/** @var Nette\Database\Context */
	protected $database;

    /** @var RouterCacheManager */
    private $routerCacheManager;

    /** @var string */
    protected $dbname;

    /** @var LinksGenerator */
    private $linksGenerator;

    /** @var Nette\Security\User */
    private $user;

    /** @var string  */
    protected $modelName;


    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
		$this->database = $database;
        $this->routerCacheManager = $routerCacheManager;
        $this->dbname = $dbname;
        $this->linksGenerator = $linksGenerator;
        $this->user = $user;

        $this->modelName = '';
    }


    public function getIdItems($id)
    {
        $name = $this->getModelName();

        return $this->database->query("
		SELECT id_items
		FROM `$name`
		WHERE id_$name = ?", $id)->fetchField();
    }

    public function getModelName()
    {
        if (!$this->modelName) {
            $rc = new \ReflectionClass($this);
            $className = $rc->getShortName();
            $this->modelName = substr(strtolower($className), 0, -5);
        }

        return $this->modelName;
    }

    public function getNextIdItems() {
        return $this->database->query('
        SELECT AUTO_INCREMENT
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = "' . $this->dbname . '"
        AND TABLE_NAME = "items"')->fetchField();
    }

    /**
     * @param $id int
     */
    public function getImagesDir($id = null)
    {
        $modelName = $this->getModelName();

        if ($id) {
            $idItems = $this->getIdItems($id);
        } else {
            $idItems = $this->getNextIdItems();
        }

        return "./images/$modelName/$idItems";
    }


    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    abstract protected function getSlug($title);

    private function getIdItemsType()
    {
        $name = $this->getModelName();

        return $this->database->query("
        SELECT id_items_type
        FROM items_type
        WHERE name = ?", $name)->fetchField();
    }

    private function filterItemValues(& $itemValues, & $values, $columns)
    {
        foreach ($columns as $column) {
            if (isset($values[$column])) {
                $itemValues[$column] = $values[$column];
                unset($values[$column]);
            }
        }
    }

    protected function addItem($values)
    {
        $itemValues = array();
        $columns = ['slug', 'title', 'comments_allowed', 'date_created', 'id_users', 'id_pages', 'id_items_type'];

        $itemValues['slug'] = $this->getSlug($values['title']);
        $itemValues['comments_allowed'] = false;
        $itemValues['date_created'] = new \DateTime();
        $itemValues['id_users'] = $this->user->getId();
        $itemValues['id_items_type'] = $this->getIdItemsType();

        $this->filterItemValues($itemValues, $values, $columns);

        $this->database->query('INSERT INTO items ', $itemValues);
        $values['id_items'] = $this->database->getInsertId();
        $this->linksGenerator->generateLinks();

        $this->makeDir("./images/" . $this->getModelName() . "/" . $values['id_items']);

        return $values;
    }

    protected function updateItem($values)
    {
        $itemValues = array();
        $columns = ['title', 'comments_allowed', 'date_updated'];

        $itemValues['date_updated'] = new \DateTime();

        $this->filterItemValues($itemValues, $values, $columns);

        $name = $this->getModelName();
        $idItems = $this->getIdItems($values["id_$name"]);

        $this->database->query('UPDATE items SET ? WHERE id_items = ?', $itemValues, $idItems);
        $this->linksGenerator->generateLinks();

        return $values;
    }

    protected function deleteItem($idItems)
    {
        $this->database->query("DELETE FROM items WHERE id_items = ?", $idItems);
        $this->linksGenerator->generateLinks();
        $this->routerCacheManager->removeItemSlug();

        $this->removeDir("./images/" . $this->getModelName() . "/" . $idItems);
    }


    /********************* SPRAVA ADRESARU *********************/

    protected function removeImage($id, $imageColumnName)
    {
        $modelName = $this->getModelName();

        $row = $this->database->query("
        SELECT id_items, $imageColumnName
        FROM $modelName
        WHERE id_$modelName = ?", $id)->fetch();

        if ($row[$imageColumnName]) {
            $filePath = "./images/$modelName/" . $row['id_items'] . "/" . $row[$imageColumnName];
            if (file_exists($filePath)) {
                unlink($filePath);
            }

            $filePath = "./images/$modelName/" . $row['id_items'] . "/thumbs/" . $row[$imageColumnName];
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }

    private function makeDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    private function removeDir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir")
                        $this->removeDir($dir."/".$object);
                    else
                        unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

}