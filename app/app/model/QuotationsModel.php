<?php

namespace App\Model;

use Nette;


class QuotationsModel extends BaseItemsModel
{
    const IMAGE_COLUMN_NAME = 'photo';


    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }
    
    public function getQuotation($idQuotations)
    {
        return $this->database->query('
		SELECT id_quotations, quotation, details, photo, priority
		FROM `quotations`
		WHERE id_quotations = ?',$idQuotations)->fetch();
    }

    public function getQuotations($paginator)
    {
        $offset = $paginator->getOffset();
        $length = $paginator->getLength();

        return $this->database->query("
		SELECT id_quotations, quotation, details, photo, priority, date_created, id_items
		FROM `quotations`
          JOIN items USING (id_items)
        ORDER BY priority ASC, date_created DESC
        LIMIT $offset, $length")->fetchAll();
    }

    public function getQuotationsCount()
    {
        return $this->database->query("
		SELECT COUNT(*)
		FROM `quotations`")->fetchField();
    }

    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . substr(Nette\Utils\Strings::webalize($title), 0, 50);
        return $slug;
    }

    /*************** INSERT, UPDATE A DELETE OPERACE ***************/
    
    public function addQuotation($values)
    {
        $values = $this->addItem($values);

        $this->database->query('INSERT INTO `quotations` ', $values);
    }
    
    public function updateQuotation($values, $idQuotations)
    {
        $values = $this->updateItem($values);

        //pokud nahrávám nový obrázek, starý vymažu, pokud existuje
        if (isset($values[self::IMAGE_COLUMN_NAME])) {
            $this->removeImage($idQuotations, self::IMAGE_COLUMN_NAME);
        }

        $this->database->query('UPDATE `quotations` SET ? WHERE id_quotations = ?', $values, $idQuotations);
    }

    public function deleteQuotation($idQuotations)
    {
        $idItems = $this->getIdItems($idQuotations);

        $this->database->query('DELETE FROM quotations WHERE id_quotations = ?', $idQuotations);

        $this->deleteItem($idItems);
    }
}
