<?php

namespace App\Model;

use Nette;


class CommentsModel extends Nette\Object
{
	/** @var Nette\Database\Context */
	private $database;

    // konstanty pro getComments(...) dosazovane za $order
    const ORDER_BY_DATE = 'date';
    const ORDER_BY_LIKES = 'likes';

    // konstanty pro rateComment(...) dosazovane za $rating
    const LIKE = 'likes = likes + 1';
    const REMOVE_LIKE = 'likes = likes - 1';
    const DISLIKE = 'dislikes = dislikes - 1';
    const REMOVE_DISLIKE = 'dislikes = dislikes + 1';

    const HIDE_COMMENT = 1;
    const SHOW_COMMENT = 0;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

    /**
     * Vraci komentare uvedene u daneho postu.
     * @param $idItems int
     * @param $order string - viz konstanty
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function getComments($idItems, $order)
    {
        return $this->database->query("
		SELECT
		    id_comments, text, date, likes, dislikes, reply_on_id_comments, nesting_in_id_comments, hidden,
		    author, author_is_admin, name AS users_name
		FROM comments
            LEFT JOIN users USING (id_users)
		WHERE id_items = ? AND nesting_in_id_comments IS NULL
		ORDER BY $order DESC ", $idItems)->fetchAll();
    }

    /**
     * Vraci reakce na komentare.
     * @param $idItems int
     * @param $order string - viz konstanty
     * @return array|Nette\Database\IRow[]
     */
    public function getCommentsReplies($idItems)
    {
        // reply_on_id_comments - ID komentare, na ktery je reagovano
        // nesting_in_id_comments - ID komentare, pod kterym (mysleno pozicne) se dany komentar vypise
        return $this->database->query('
		SELECT
			c1.id_comments, c1.author, c1.author_is_admin, u1.name AS users_name, c1.text, c1.date, c1.likes, c1.dislikes, c1.hidden,
			c1.nesting_in_id_comments, c1.reply_on_id_comments, c2.author AS commented_author, u2.name AS commented_users_name
		FROM comments c1
            LEFT JOIN users u1 ON (u1.id_users = c1.id_users)
			JOIN comments c2 ON (c2.id_comments = c1.reply_on_id_comments)
            LEFT JOIN users u2 ON (u2.id_users = c2.id_users)
		WHERE c1.id_items = ? AND c1.nesting_in_id_comments IS NOT NULL
		ORDER BY nesting_in_id_comments DESC , date ASC', $idItems)->fetchAll();
    }

    /**
     * Vrati dany komentar.
     * @param $idComments int
     * @return Nette\Database\ResultSet
     */
    public function getComment($idComments)
    {
        return $this->database->query('
		SELECT
		    id_comments, text, date, likes, dislikes, reply_on_id_comments, nesting_in_id_comments, hidden,
		    author, author_is_admin, name AS users_name
		FROM comments
            LEFT JOIN users USING (id_users)
		WHERE id_comments = ?', $idComments);
    }

    /**
     * Kontrola, jestli dany koment existuje.
     * @param $idComments int
     * @return FALSE|mixed
     */
    public function commentExists($idComments)
    {
        return $this->database->query('
		SELECT id_comments
		FROM comments
		WHERE id_comments = ?',$idComments)->fetch();
    }

    /**
     * Prida komentar.
     * @param $values
     */
    public function addComment($values)
    {
        $this->database->query('
		INSERT INTO comments ',$values);
    }

    /**
     * Smaze dany komentar.
     * @param $idComments int
     */
    public function deleteComment($idComments)
    {
        $this->database->query('
		DELETE FROM comments
		WHERE id_comments = ?',$idComments);
    }

    /**
     * Skryje/zobrazi dany komentar.
     * @param $action int
     * @param $idComments int
     */
    public function switchHidingComment($action, $idComments)
    {
        $this->database->query('
        UPDATE comments
        SET hidden = ?
		WHERE id_comments = ?',$action,$idComments);
    }

    /**
     * Like/dislike daneho komentare.
     * @param $idComments int
     * @param $rating string - viz konstanty
     */
    public function rateComment($idComments, $rating)
    {
        $this->database->query('
        UPDATE comments
        SET ' . $rating . '
		WHERE id_comments = ?',$idComments);
    }

}
