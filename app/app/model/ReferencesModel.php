<?php

namespace App\Model;

use Nette;


class ReferencesModel extends BaseItemsModel
{
    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }
    
    public function getReference($idReferences)
    {
        return $this->database->query('
		SELECT id_references, perex, date, source, content, slug, title, comments_allowed, id_pages
		FROM `references`
          JOIN items USING (id_items)
		WHERE id_references = ?',$idReferences)->fetch();
    }

    public function getReferences($paginator)
    {
        $offset = $paginator->getOffset();
        $length = $paginator->getLength();

        return $this->database->query("
		SELECT id_references, perex, date, source, slug, title, id_pages
		FROM `references`
          JOIN items USING (id_items)
        ORDER BY date DESC
        LIMIT $offset, $length")->fetchAll();
    }

    public function getReferencesCount()
    {
        return $this->database->query("
		SELECT COUNT(*)
		FROM `references`")->fetchField();
    }


    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . substr(Nette\Utils\Strings::webalize($title), 0, 50);
        return $slug;
    }


    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    public function addReference($values)
    {
        $values = $this->addItem($values);

        $this->database->query('INSERT INTO `references` ', $values);
        //$this->linksGenerator->generateLinks();
    }
    
    public function updateReference($values, $idReferences)
    {
        $values = $this->updateItem($values);

        $this->database->query('UPDATE `references` SET ? WHERE id_references = ?', $values, $idReferences);
        //$this->linksGenerator->generateLinks();
        //$this->routerCacheManager->removeReferenceSlug();
    }

    public function deleteReference($idReferences)
    {
        $idItems = $this->getIdItems($idReferences);

        $this->database->query("DELETE FROM `references` WHERE id_references = ?", $idReferences);

        $this->deleteItem($idItems);
    }
}
