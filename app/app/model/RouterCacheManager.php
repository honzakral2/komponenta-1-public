<?php

namespace App\Model;

use Nette,
    Nette\Caching\Cache;


class RouterCacheManager extends Nette\Object
{
    /** @var Nette\Database\Context */
    private $database;

    /** @var Cache */
    private $cache;

    /** @var array */
    private $dependencies;

    const PAGE_SLUGS = 'pages';
    const HOMEPAGE = 'homepage';
    const ITEMS_SLUG = 'items';


    public function __construct(Nette\Database\Context $database, Nette\Caching\Storages\FileStorage $storage)
    {
        $this->database = $database;

        $this->cache = new Cache($storage, 'Router');

        $this->dependencies = array(
            Cache::EXPIRE => '1 days'
        );
    }


    /**
     * Vraci vychozi stranku neboli homepage - http://www.domena.cz/
     * @param Nette\Database\Context $database
     * @return FALSE|mixed
     */
    public function getHomepageId()
    {
        // TODO Je treba vyresit, co se stane, kdyz doposud nebyla nastavena vychozi stranka !!!

        $homepageId = $this->cache->load(self::HOMEPAGE);

        if ($homepageId) {
            return $homepageId;
        } else {
            $homepageId = $this->database->query('
            SELECT id_pages
            FROM pages
            WHERE homepage = 1')->fetchField();

            $this->cache->save(self::HOMEPAGE, $homepageId, $this->dependencies);

            return $homepageId;
        }
    }

    /**
     * @param $slug string
     * @param $slugType string self::POST_SLUGS|self::PAGE_SLUGS
     * @return int|null
     */
    public function getId($slug, $slugType)
    {
        if (!$slugType || !$slug)
            return null;

        $slugs = $this->cache->load($slugType);

        if ($slugs) {
            $id = array_search($slug, $slugs);
            if ($id)
                return $id;
        }

        if ($slugType === self::PAGE_SLUGS) {
            $id = $this->database->query('
            SELECT id_pages
            FROM pages
            WHERE slug = ?', $slug)->fetchField();

        } else {
            $id = $this->database->query('
            SELECT id_items
            FROM items
              JOIN items_type USING(id_items_type)
            WHERE slug = ? AND is_linkable = 1', $slug)->fetchField();
        }

        return $id ? $id : null;
    }

    /**
     * @param $id int
     * @param $slugType string self::ITEMS_SLUG|self::PAGE_SLUGS
     * @return string|NULL
     */
    public function getSlug($id, $slugType)
    {
        if (!$slugType || !$id)
            return null;

        if (!is_numeric($id)) {
            return $id;
        }

        $slugs = $this->cache->load($slugType);

        if (isset($slugs[$id])) {
            return $slugs[$id];
        } else {

            if ($slugType === self::PAGE_SLUGS) {
                $slug = $this->database->query('
                SELECT slug
                FROM pages
                WHERE id_pages = ?', $id)->fetchField();

            } else {
                $slug = $this->database->query('
                SELECT slug
                FROM items
                  JOIN items_type USING(id_items_type)
                WHERE id_items = ? AND is_linkable = 1', $id)->fetchField();
            }

            $slugs[$id] = $slug;
            $this->cache->save($slugType, $slugs, $this->dependencies);

            return $slug;
        }
    }


    /********************* METODY PRO MAZANI CACHE *********************/

    public function removeItemSlug()
    {
        $postSlugs = $this->cache->load(self::ITEMS_SLUG);

        if ($postSlugs)
            $this->cache->remove(self::ITEMS_SLUG);
    }

    public function removePageSlug()
    {
        $pageSlugs = $this->cache->load(self::PAGE_SLUGS);

        if ($pageSlugs)
            $this->cache->remove(self::PAGE_SLUGS);
    }

    public function removeHomepage()
    {
        $homepage = $this->cache->load(self::HOMEPAGE);

        if ($homepage)
            $this->cache->remove(self::HOMEPAGE);
    }

}
