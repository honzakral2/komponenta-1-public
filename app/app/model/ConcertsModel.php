<?php

namespace App\Model;

use Nette;


class ConcertsModel extends BaseItemsModel
{
    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }
    
    public function getConcert($idConcerts)
    {
        return $this->database->query('
		SELECT
		    id_concerts, title, title_en, date, description, description_en, place, ticket_url,
		    slug, comments_allowed, id_pages
		FROM concerts
		    JOIN items USING(id_items)
		WHERE id_concerts = ?',$idConcerts)->fetch();
    }


    public function getPastConcerts($paginator)
    {
        $offset = $paginator->getOffset();
        $length = $paginator->getLength();

        return $this->database->query("
		SELECT
		    id_concerts, title, title_en, date, description, description_en, place, ticket_url,
		    slug, comments_allowed, id_pages
		FROM concerts
		    JOIN items USING(id_items)
		WHERE date < NOW()
        ORDER BY date DESC
        LIMIT $offset, $length")->fetchAll();
    }

    public function getFutureConcerts($limit = 0)
    {
        if ($limit) {
            $limit = "LIMIT $limit";
        } else {
            $limit = "";
        }

        return $this->database->query("
		SELECT
		    id_concerts, title, title_en, date, description, description_en, place, ticket_url,
		    slug, comments_allowed, id_pages
		FROM concerts
		    JOIN items USING(id_items)
		WHERE date >= NOW()
        ORDER BY date ASC
        $limit")->fetchAll();
    }

    public function getPastConcertsCount()
    {
        return $this->database->query("
		SELECT COUNT(*)
		FROM concerts
        WHERE date < NOW()")->fetchField();
    }

    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . substr(Nette\Utils\Strings::webalize($title), 0, 50);
        return $slug;
    }

    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    public function addConcert($values)
    {
        $values = $this->addItem($values);

        $this->database->query('INSERT INTO concerts ', $values);
    }
    
    public function updateConcert($values, $idConcerts)
    {
        $values = $this->updateItem($values);

        $this->database->query('UPDATE concerts SET ? WHERE id_concerts = ?', $values, $idConcerts);
    }

    public function deleteConcert($idConcerts)
    {
        $idItems = $this->getIdItems($idConcerts);

        $this->database->query("DELETE FROM concerts WHERE id_concerts = ?", $idConcerts);
        $this->deleteItem($idItems);
    }
}
