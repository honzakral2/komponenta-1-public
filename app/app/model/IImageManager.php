<?php

namespace App\Model;

use Nette;

interface IImageManager
{
    /** @return ImageManager */
    function create($dirPath, $newHTML, $oldHTML);
}
