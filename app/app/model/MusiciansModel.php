<?php

namespace App\Model;

use Nette;


class MusiciansModel extends BaseItemsModel
{
    const IMAGE_COLUMN_NAME = 'photo';


    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }
    
    public function getMusician($idMusicians)
    {
        return $this->database->query('
		SELECT id_musicians, name, instrument, photo, sort
		FROM `musicians`
		    JOIN items USING(id_items)
		WHERE id_musicians = ?',$idMusicians)->fetch();
    }

    public function getMusicians()
    {
        return $this->database->query("
		SELECT id_musicians, name, instrument, photo, sort, id_items
		FROM `musicians`
		    JOIN items USING(id_items)
        ORDER BY sort")->fetchAll();
    }


    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . substr(Nette\Utils\Strings::webalize($title), 0, 50);
        return $slug;
    }

    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    public function addMusician($values)
    {
        $values = $this->addItem($values);

        $sortLast = $this->database->query('SELECT MAX(sort)+1 FROM musicians')->fetchField();
        $values['sort'] = $sortLast === null ? 0 : $sortLast;
        $this->database->query('INSERT INTO `musicians` ', $values);
    }
    
    public function updateMusician($values, $idMusicians)
    {
        $values = $this->updateItem($values);

        //pokud nahrávám nový obrázek, starý vymažu, pokud existuje
        if (isset($values[self::IMAGE_COLUMN_NAME])) {
            $this->removeImage($idMusicians, self::IMAGE_COLUMN_NAME);
        }

        $this->database->query('UPDATE `musicians` SET ? WHERE id_musicians = ?', $values, $idMusicians);
    }

    public function deleteMusician($idMusicians)
    {
        $idItems = $this->getIdItems($idMusicians);
        $sort = $this->database->query('SELECT sort FROM musicians WHERE id_musicians = ?', $idMusicians)->fetchField();

        $this->database->query('DELETE FROM musicians WHERE id_musicians = ?', $idMusicians);
        //uprava poradi muzikantu
        $this->database->query('UPDATE musicians SET sort = sort - 1 WHERE sort > ? ORDER BY sort ASC', $sort);

        $this->deleteItem($idItems);
    }

    
    // nastaví pořadí muzikanta na požadovanou hodnotu. Navíc zajišťuje unikátnost TODO unique index?
    // sloupce sort (tj. že na jednom pořadovém čísle nebudou dva a více muzikanti
    public function setSort($idMusicians, $sort)
    {
        $sortCurrent = $this->database->query('SELECT sort FROM musicians WHERE id_musicians = ?', $idMusicians)->fetchField();
        $this->database->query('UPDATE musicians SET sort = -1 WHERE id_musicians = ?', $idMusicians);

        //muzikant bude mít nižší pořadové číslo než doposud
        if ($sort < $sortCurrent) {
            $this->database->query('UPDATE musicians SET sort = sort + 1 WHERE sort >= ? AND sort < ? ORDER BY sort DESC', $sort, $sortCurrent);
        //muzikant bude mít vyšší pořadové číslo než doposud
        } else if ($sort > $sortCurrent) {
            $this->database->query('UPDATE musicians SET sort = sort - 1 WHERE sort > ? AND sort <= ? ORDER BY sort ASC', $sortCurrent, $sort);
        }

        $this->database->query('UPDATE musicians SET sort = ? WHERE id_musicians = ?', $sort, $idMusicians);
        //pokud je pořadové číslo stejné jako doposud, nic se nestane
    }
}
