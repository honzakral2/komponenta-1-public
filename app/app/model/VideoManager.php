<?php

namespace App\Model;

use Nette,
    Nette\Utils\Json,
    Nette\Utils\JsonException;


class VideoManager extends Nette\Object
{

    const CHANNEL_ID = "UCR6b8DQshi5VRbStfA7lyGg";
    const API_KEY = "AIzaSyDonwQdsUeGG9F8broP8ejwR2wXl9xEcW0";
    const MAX_RESULTS = 3;
    
    private $videoIdsArray = array();
    
    public function __construct()
    {

    }
    
    public function getVideos()
    {
        $url = "https://www.googleapis.com/youtube/v3/search?key=<api_key>&channelId=<channel_id>&part=snippet,id&order=date&maxResults=<max_results>&type=video";
        $url =  str_replace("<channel_id>", self::CHANNEL_ID,
            str_replace("<max_results>", self::MAX_RESULTS,
                str_replace("<api_key>", self::API_KEY, $url)));

        //Nette\Diagnostics\Debugger::log($url);

        error_reporting(~0);
        ini_set('display_errors', 1);
        $json = file_get_contents($url);
        foreach (Json::decode($json)->items as $video) {
            //Nette\Diagnostics\Debugger::log('aaa'.$video->id->videoId);
            array_push($this->videoIdsArray, $video->id->videoId);
        }

        //Nette\Diagnostics\Debugger::log($this->videoIdsArray);


        return $this->videoIdsArray;
    }

}

