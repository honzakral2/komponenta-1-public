<?php

namespace App\Model;

use Nette;


class MapManager extends Nette\Object
{
    /** @var Nette\Database\Context */
    private $database;
    
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
    
    //ulozi placeId do databaze. PlaceId slouzi k zobrazeni adresy na mape (pres google maps api)
    public function saveAddressPlaceId($placeId) {
        if ($this->getCurrentPlaceId()) {
            $this->database->query("UPDATE address SET place_id = ?", $placeId);
        } else {
            $this->database->query("INSERT INTO address (place_id) VALUES (?)", $placeId);
        }
    }
    
    //vrati placeID z DB, pokud je zadane. Pokud ne, vrati null
    public function getCurrentPlaceId() {
        return $this->database->query("SELECT place_id FROM address")->fetchField(0);
    }
  
}
