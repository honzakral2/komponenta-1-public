<?php

namespace App\Model;

use Nette;


class AlbumsModel extends BaseItemsModel
{
    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }


    /********************* OPERACE TYKAJICI SE ALB *********************/

    public function getAlbum($idAlbums)
    {
        return $this->database->query('
		SELECT id_albums, description, album_type, id_items, title, slug, id_pages
		FROM albums
          JOIN items USING (id_items)
		WHERE id_albums = ?',$idAlbums)->fetch();
    }

    /**
     * @param $referenceId int
     * @param $referenceName string
     * @return FALSE|mixed
     */
    public function getAlbumsId($referenceId, $referenceName)
    {
        return $this->database->query("
		SELECT id_albums
		FROM albums
          JOIN items USING (id_items)
		WHERE id_$referenceName = ?", $referenceId)->fetchField();
    }

    /**
     * @param $idPages int
     * @param $paginator Nette\Utils\Paginator
     * @return array
     */
    public function getAlbums($idPages, $paginator)
    {
        $offset = $paginator->getOffset();
        $length = $paginator->getLength();

        return $this->database->query("
		SELECT id_albums, description, album_type, id_items, slug, title, id_pages
		FROM albums
          JOIN items USING (id_items)
		WHERE id_pages = ?
		ORDER BY title
		LIMIT $offset, $length", $idPages)->fetchAll();
    }

    public function getAlbumsCount($idPages)
    {
        return $this->database->query('
        SELECT COUNT(*)
        FROM albums
          JOIN items USING (id_items)
        WHERE id_pages = ?', $idPages)->fetchField();
    }

    public function getPreviewImages($idPages)
    {
        $previewImages = array();

        $albums = $this->database->query("
		SELECT id_albums, title
		FROM albums
		  JOIN items USING(id_items)
		WHERE id_pages = ?
		ORDER BY title", $idPages)->fetchPairs();

        foreach ($albums as $albumId => $albumName) {
            $previewImages[$albumId] = $this->database->query('
            SELECT filename
            FROM photos
		      JOIN items USING(id_items)
		      JOIN albums USING(id_items)
            WHERE id_albums = ?
            ORDER BY id_photos DESC
            LIMIT 4', $albumId)->fetchAll();
        }

        return $previewImages;
    }


    protected function getSlug($title, $idAlbums = null)
    {
        $slug = null;

        if ($idAlbums) {
            $slug = $this->database->query('
            SELECT slug
            FROM albums
              JOIN items USING (id_items)
            WHERE id_albums = ?', $idAlbums)->fetchField();
        }

        if (!$slug) {
            $idItems = $this->getNextIdItems();
            $slug = $idItems . '-' . Nette\Utils\Strings::webalize($title);
        }

        return $slug;
    }


    /****** INSERT, UPDATE A DELETE OPERACE ******/

    /**
     * @param $album array
     * @param null $fileNames array
     * @return FALSE|mixed
     */
    public function insertFotoAlbum($album, $fileNames = array())
    {
        $album = $this->addItem($album);

        $this->database->query('INSERT INTO albums ',$album);

        if ($fileNames) {
            $this->insertImages($album['id_items'], $fileNames);
        }
    }

    public function insertVideoAlbum($album)
    {
        $album = $this->addItem($album);

        $this->database->query('INSERT INTO albums ',$album);
    }


    /**
     * @param $values Nette\Utils\ArrayHash
     * @param $idAlbums int
     */
    public function updateAlbum($values)
    {
        $idAlbums = $values['id_albums'];

        $values = $this->updateItem($values);
        unset($values['id_albums']);

        $this->database->query('UPDATE albums SET ? WHERE id_albums = ?', $values, $idAlbums);
    }

    /**
     * Odstrani album vcetne k nemu spjatych fotek a adresaru.
     * @param $idAlbums int
     */
    public function deleteAlbum($idAlbums)
    {
        $idItems = $this->getIdItems($idAlbums);

        $this->database->query('DELETE FROM photos WHERE id_items = ?', $idItems);
        $this->database->query('DELETE FROM albums WHERE id_albums = ?', $idAlbums);

        $this->deleteItem($idItems);
    }


    /********************* OPERACE TYKAJICI SE FOTEK *********************/

    /**
     * @param $idAlbums int
     * @param $paginator Nette\Utils\Paginator
     * @return array
     */
    public function getImages($idAlbums, $paginator = null)
    {
        $page = '';

        if ($paginator) {
            $offset = $paginator->getOffset();
            $length = $paginator->getLength();
            $page = "LIMIT $offset, $length";
        }

        return $this->database->query("
		SELECT id_photos, filename
		FROM photos
		  JOIN items USING(id_items)
		  JOIN albums USING(id_items)
		WHERE id_albums = ?
        ORDER BY id_photos DESC
        $page", $idAlbums)->fetchPairs();
    }

    public function getLastImages()
    {
        return $this->database->query("
		SELECT id_items, filename
		FROM photos
		  JOIN items USING(id_items)
		  JOIN albums USING(id_items)
		WHERE album_type = 0
        ORDER BY id_photos DESC
        LIMIT 4")->fetchAll();
    }

    public function getImagesCount($idAlbums)
    {
        return $this->database->query('
        SELECT COUNT(*)
        FROM photos
		  JOIN items USING(id_items)
		  JOIN albums USING(id_items)
        WHERE id_albums = ?', $idAlbums)->fetchField();
    }


    /****** INSERT A DELETE OPERACE ******/

    public function insertImages($idItems, $fileNames)
    {
        foreach ($fileNames as $fileName) {
            $this->database->query('
            INSERT INTO photos (filename, id_items)
            VALUES (?, ?)', $fileName, $idItems);
        }
    }

    public function deleteImages($selectedImages)
    {
        foreach ($selectedImages as $idPhoto) {

            $photo = $this->database->query('
            SELECT filename, id_items
            FROM photos
            WHERE id_photos = ?', $idPhoto)->fetch();

            if ($photo) {
                $this->database->query('DELETE FROM photos WHERE id_photos = ?', $idPhoto);

                $this->deletePhoto($photo->id_items, $photo->filename);
            }
        }
    }
    
    private function deletePhoto($idItems, $filename)
    {
        $modelName = $this->getModelName();

        $filePath = "./images/$modelName/" . $idItems . "/" . $filename;
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        $filePath = "./images/$modelName/" . $idItems . "/thumbs/" . $filename;
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }


    /********************* OPERACE TYKAJICI SE VIDEI *********************/

    public function getVideo($idVideo)
    {
        return $this->database->query('
		SELECT id_photos
		FROM photos
		WHERE id_photos = ?', $idVideo)->fetch();
    }

    public function getVideoByName($filename)
    {
        return $this->database->query('
		SELECT id_photos
		FROM photos
		WHERE filename = ?', $filename)->fetch();
    }


    /****** INSERT A DELETE OPERACE ******/

    public function insertVideo($values)
    {
        $values['id_items'] = $this->getIdItems($values['id_albums']);
        unset($values['id_albums']);

        $this->database->query('INSERT INTO photos ', $values);
    }

    public function deleteVideo($idVideo)
    {
        $this->database->query('DELETE FROM photos WHERE id_photos = ?', $idVideo);
    }
}
