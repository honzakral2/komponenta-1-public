<?php

namespace App\Model;

use Nette;


class ItemsModel extends Nette\Object
{
	/** @var Nette\Database\Context */
	protected $database;

    const ALLOW_COMMENTS = 1;
    const DENY_COMMENTS = 0;


    public function __construct(Nette\Database\Context $database)
    {
		$this->database = $database;
    }


    public function getItemType($idItems)
    {
        return $this->database->query('
		SELECT name
		FROM items
		    JOIN items_type USING(id_items_type)
		WHERE id_items = ?',$idItems)->fetchField();
    }

    /**
     * @param $idItems
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function getItem($idItems, $itemType)
    {
        return $this->database->query('
		SELECT *
		FROM `'  . $itemType . '`
		    JOIN items USING(id_items)
		WHERE id_items = ?',$idItems)->fetch();
    }


    /******************* POVOLENI/ZAKAZ KOMENTARU *******************/

    /**
     * Umozni/znemozni zobrazeni komentaru u daneho prispevku.
     * @param $action int
     * @param $idItems int
     */
    public function switchCommentsAllowed($action, $idItems)
    {
        $this->database->query('
        UPDATE items
        SET comments_allowed = ?
		WHERE id_items = ?',$action,$idItems);
    }

    /**
     * Vrati, jestli jsou u daneho prispevku povoleny komentare.
     * @param $idItems int
     * @return FALSE|mixed
     */
    public function getCommentAllowed($idItems)
    {
        return $this->database->query('
        SELECT comments_allowed
        FROM items
        WHERE id_items = ?',$idItems)->fetchField();
    }
}