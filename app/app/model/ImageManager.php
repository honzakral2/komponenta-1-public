<?php

namespace App\Model;

use Nette,
    DOMDocument,
    Nette\Utils\Image;


class ImageManager extends Nette\Object
{
    /** @var string */
    private $rootDir;

    /** @var int */
    private $pageContentWidth;


    /** @var string */
    private $baseDirPath;

    /** @var string */
    private $newHTML;

    /** @var DOMDocument */
    private $newDOM;

    /** @var string */
    private $oldHTML;

    /** @var DOMDocument */
    private $oldDOM;



    public function __construct($rootDir, $pageContentWidth, $dirPath, $newHTML, $oldHTML)
    {
        // Hodnoty predavane v config.neon
        $this->rootDir = ( $_SERVER['SERVER_NAME'] == 'localhost' ? "$rootDir/www" : "/www" );
        $this->pageContentWidth = $pageContentWidth;


        $this->makeImageDirs($dirPath);

        if ($newHTML) {
            $this->newHTML = $newHTML;
            $this->newDOM = $this->createDOM($newHTML);
        } else {
            $this->newHTML = '';
            $this->newDOM = null;
        }

        if ($oldHTML) {
            $this->oldHTML = $oldHTML;
            $this->oldDOM = $this->createDOM($oldHTML);
        } else {
            $this->oldHTML = '';
            $this->oldDOM = null;
        }

        $this->processContent();
    }


    public function getContent()
    {
        return $this->newHTML;
    }

    private function processContent()
    {
        // upravim co bylo pridane
        if ($this->newDOM) {
            $newImages = $this->getImages($this->newDOM);
            $oldImages = $this->getImages($this->oldDOM);

            $missingImages = $this->getMissingImages($oldImages, $newImages);
            $this->removeSelectedImages($missingImages);

            foreach ($newImages as $newImage) {
                $oldImage = $this->findEqualOld($newImage, $oldImages);

                if ($this->imageChanged($oldImage, $newImage)) {
                    $imagePath = $this->makeThumbnail($newImage);
                    $this->wrapLinkAround($newImage, $imagePath);
                }
            }

            $this->newHTML = $this->newDOM->saveHTML();
        }

        // cely puvodni obsah byl odstranen
        if (!$this->newDOM && $this->oldDOM) {
            $this->removeDir($this->baseDirPath);
        }
    }


    private function makeThumbnail($newImage)
    {
        /***************** ZISKANI CESTY K OBRAZKU ****************/

        // pokud je obrazek obaleny odkazem, budu pracovat s odkazovanym (pravdepodobne ma vyssi rozliseni)
        if ($newImage->parentNode->tagName == "a" &&
            $newImage->parentNode->getAttribute("class") == "fresco image-wrapper" &&
            $this->isImage($newImage->parentNode->getAttribute("href"))
        ) {
            $origImagePath = $newImage->parentNode->getAttribute("href");
        } else {
            $origImagePath = $newImage->getAttribute("src");
        }

        // obrazek je na jinem webu - ulozim ho k nam
        if ($this->isOutboundLink($origImagePath)) {
            $originalImage = Image::fromFile($origImagePath);
            $extension = pathinfo(strtok($origImagePath, '?'))['extension'];
            $fileName = Nette\Utils\Random::generate(20) . "." . $extension;

            $originalImage->save("$this->baseDirPath/$fileName", 75);

        } else {
            $count = mb_strlen($this->rootDir);
            $origImagePath = "." . substr($origImagePath, $count);
            $fileName = pathinfo($origImagePath)['basename'];

            $originalImage = Image::fromFile($origImagePath);

            // obrazek je ulozen jinde, nez ma byt - presunu ho
            if ($origImagePath !== "$this->baseDirPath/$fileName") {
                rename($origImagePath, "$this->baseDirPath/$fileName");
            }
        }

        /********************* RESIZE OBRAZKU *********************/

        $newThumbWidth = $newImage->getAttribute("width");
        $newThumbHeight = $newImage->getAttribute("height");

        if ($newThumbWidth && $newThumbHeight) {
            if ($newThumbWidth > $this->pageContentWidth) {
                $newThumbHeight = $newThumbHeight * ($this->pageContentWidth/$newThumbWidth);
                $newThumbWidth = $this->pageContentWidth;
            }

            if ($originalImage->width != $newThumbWidth || $originalImage->height != $newThumbHeight) {
                $originalImage->resize($newThumbWidth, $newThumbHeight);
            }

            $originalImage->save("$this->baseDirPath/thumbs/$fileName", 75);
            $newImage->setAttribute("width", $newThumbWidth);
            $newImage->setAttribute("height", $newThumbHeight);
        }

        $baseDirPath = substr("$this->baseDirPath", 2);
        $newImage->setAttribute("src", "$this->rootDir/$baseDirPath/thumbs/$fileName");

        return "$this->rootDir/$baseDirPath/$fileName";
    }


    private function findEqualOld($newImage, $oldImages)
    {
        foreach ($oldImages as $oldImage) {
            $newImageName = basename($newImage->getAttribute("src"));
            $oldImageName = basename($oldImage->getAttribute("src"));

            if ($oldImageName === $newImageName) {
                return $oldImage;
            }
        }

        return null;
    }

    private function imageChanged($oldImage, $newImage)
    {
        if (!$oldImage) {
            return true;

        } else {
            // ma stejny odkaz
            if ($oldImage->parentNode->tagName === "a" && $newImage->parentNode->tagName === "a" &&
                ($oldImage->parentNode->getAttribute("href") === $newImage->parentNode->getAttribute("href"))) {

                // ma stejne rozmery
                if ($oldImage->getAttribute("width") == $newImage->getAttribute("width") &&
                    $oldImage->getAttribute("height") == $newImage->getAttribute("height")) {
                    return false;
                }
            }
        }

        // jinak doslo ke zmene
        return true;
    }


    /************ ZABALI NAHLED ODKAZEM NA PUVODNI OBRAZEK *************/

    private function isWrapperNeeded($image)
    {
        // obrazek neni obalen odkazem
        if ($image->parentNode->tagName != "a") {
            return true;

        // obrazek neni obalen odkazem na jinou stranku a odkazuje na obrazek
        } else if (!$this->isOutboundLink($image->parentNode->getAttribute("href")) &&
            $this->isImage($image->parentNode->getAttribute("href"))) {
            return true;

        } else {
            return false;
        }
    }

    private function wrapLinkAround($image, $imagePath)
    {
        if ($this->isWrapperNeeded($image)) {
            //Create new wrapper
            $imageWrapper = $this->newDOM->createElement('a');
            $imageWrapper->setAttribute("href", $imagePath);
            //propojeni obrazku do jedne galerie
            $imageWrapper->setAttribute("data-fresco-group", "gallery");
            $imageWrapper->setAttribute("class", "fresco image-wrapper");

            //Replace image with this wrapper
            $image->parentNode->replaceChild($imageWrapper, $image);
            //Append this image to wrapper div
            $imageWrapper->appendChild($image);

        } else {
            $image->parentNode->removeAttribute("data-fresco-group");
            $image->parentNode->removeAttribute("class");
        }
    }


    /******** VRACI, KTERE OBRAZKY BYLY (VE WYSIWYGU) VYMAZANY *********/

    private function getMissingImages($oldImages, $newImages) {
        //pokud v puvodnim postu nic nebylo, zadny obrazek smazan nebyl
        if ($oldImages) {
            $oldImagesNames = $this->getImagesNames($oldImages);
            $newImagesNames = $this->getImagesNames($newImages);

            return array_diff($oldImagesNames, $newImagesNames);

        } else {
            return array();
        }
    }

    private function getImagesNames($imageElements)
    {
        $imageNames = array();
        foreach ($imageElements as $element) {
            $imageNames[] = basename($element->getAttribute("src"));
        }

        return $imageNames;
    }


    /************** VYTVARENI A MAZANI SOUBORU A ADRESARU **************/

    private function makeImageDirs($dirPath)
    {
        $dirPath = $dirPath . '/content';
        $this->makeDir($dirPath);

        $thumbDirPath = $dirPath . '/thumbs';
        $this->makeDir($thumbDirPath);

        $this->baseDirPath = $dirPath;
    }

    private function makeDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    private function removeSelectedImages($images) {

        foreach ($images as $image) {
            $filePath = "$this->baseDirPath/$image";
            if (file_exists($filePath)) {
                unlink($filePath);
            }
            $filePath = "$this->baseDirPath/thumbs/$image";
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }

    private function removeDir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir")
                        $this->removeDir($dir."/".$object);
                    else
                        unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }


    /********************* OSTATNI POMOCNE METODY **********************/

    /**
     * @param $HTML string
     * @return DOMDocument
     */
    private function createDOM($HTML)
    {
        // http://stackoverflow.com/questions/9149180/domdocumentloadhtml-error
        $DOM = new DOMDocument;
        $encodedHTML = mb_convert_encoding($HTML, 'HTML-ENTITIES', 'UTF-8');
        @$DOM->loadHTML($encodedHTML, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        return $DOM;
    }

    /**
     * @param $DOM DOMDocument
     * @return array
     */
    private function getImages($DOM)
    {
        if ($DOM) {
            return $DOM->getElementsByTagName("img");
        }
        return array();
    }

    // zjisti, zda $href vede na stranku mimo nasi domenu
    private function isOutboundLink($href) {
        $parsedUrl = parse_url($href);
        if (isset($parsedUrl['host'])) {
            if ($parsedUrl['host'] != $_SERVER['SERVER_NAME']) {
                return true;
            }
        }
        return false;
    }

    // zjisti, zda se na dane adrese vyskytuje obrazek
    private function isImage($href) {
        $pathInfo = pathinfo($href);
        if (array_key_exists('extension', $pathInfo)) {
            $extension = strtolower($pathInfo['extension']);
            $imageExtensions = array('jpg', 'jpeg', 'png', 'gif', 'svg', 'bmp', 'ico', 'tif', 'tiff');
            return in_array($extension, $imageExtensions);
        } else {
            return false;
        }
    }

}