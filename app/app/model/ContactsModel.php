<?php

namespace App\Model;

use Nette;


class ContactsModel extends BaseItemsModel
{
    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }
    
    public function getContact($idContacts)
    {
        return $this->database->query('
		SELECT id_contacts, name, function, mail, phone, sort
		FROM contacts
		WHERE id_contacts = ?',$idContacts)->fetch();
    }

    public function getContacts()
    {
        return $this->database->query("
		SELECT id_contacts, name, function, mail, phone, sort
		FROM contacts
        ORDER BY sort")->fetchAll();
    }

    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . Nette\Utils\Strings::webalize($title);
        return $slug;
    }

    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    public function addContact($values)
    {
        $values = $this->addItem($values);

        $sortLast = $this->database->query('SELECT MAX(sort)+1 FROM contacts')->fetchField();
        $values['sort'] = $sortLast === null ? 0 : $sortLast;
        $this->database->query('INSERT INTO contacts ', $values);
    }
    
    public function updateContact($values, $idContacts)
    {
        $values = $this->updateItem($values);

        $this->database->query('UPDATE contacts SET ? WHERE id_contacts = ?', $values, $idContacts);
    }

    public function deleteContact($idContacts)
    {
        $idItems = $this->getIdItems($idContacts);

        $this->database->query("DELETE FROM contacts WHERE id_contacts = ?", $idContacts);

        $this->deleteItem($idItems);
    }

    
    //nastaví pořadí kontaktu na požadovanou hodnotu. Navíc zajišťuje unikátnost sloupce sort (tj. že na jednom pořadovém čísle nebudou dva a více kontakty
    public function setSort($idContacts, $sort)
    {
        $sortCurrent = $this->database->query('SELECT sort FROM contacts WHERE id_contacts = ?', $idContacts)->fetchField();
        $this->database->query('UPDATE contacts SET sort = -1 WHERE id_contacts = ?', $idContacts);

        //muzikant bude mít nižší pořadové číslo než doposud
        if ($sort < $sortCurrent) {
            $this->database->query('UPDATE contacts SET sort = sort + 1 WHERE sort >= ? AND sort < ? ORDER BY sort DESC', $sort, $sortCurrent);
        //muzikant bude mít vyšší pořadové číslo než doposud
        } else if ($sort > $sortCurrent) {
            $this->database->query('UPDATE contacts SET sort = sort - 1 WHERE sort > ? AND sort <= ? ORDER BY sort ASC', $sortCurrent, $sort);
        }

        $this->database->query('UPDATE contacts SET sort = ? WHERE id_contacts = ?', $sort, $idContacts);
        //pokud je pořadové číslo stejné jako doposud, nic se nestane
    }    
    
}
