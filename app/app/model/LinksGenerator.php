<?php

namespace App\Model;

use Nette,
    Nette\Utils\Json;


class LinksGenerator extends Nette\Object
{
    /** @var \Nette\Http\Request */
    private $httpRequest;

    /** @var Nette\Database\Context */
    private $database;

    /** @var string */
    private $hostname;


    const FILENAME = '../www/js/links.json';


    public function __construct(Nette\Database\Context $database, Nette\Http\IRequest $httpRequest)
    {
        $this->database = $database;
        $this->httpRequest = $httpRequest;
        $this->hostname = $this->getHostname();
    }


    /**
     * @return string
     */
    private function getHostname()
    {
        $url = $this->httpRequest->getUrl();
        return $url->host;
    }

    /**
     * @throws Nette\Utils\JsonException
     */
    public function generateLinks()
    {
        $links = $this->getMenuLinks();

        $items = $this->getLinkableItems();
        foreach ($items as $idPages => $menuSectionName) {
            $itemsLinks = $this->getItemsLinks($menuSectionName, $idPages);
            $links[] = [
                'title' =>  ucfirst($menuSectionName) . ' (položky)',
                'menu' => $itemsLinks
            ];
        }

        $jsonLinks = Json::encode($links, Json::PRETTY);

        $this->saveLinksToFile($jsonLinks);
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        $links = array();

        $menuLinks = $this->getMenuLinks();
        foreach ($menuLinks as $link) {
            $links[$link->value] = $link->title;
        }

        $linkableItems = $this->getLinkableItems();
        foreach ($linkableItems as $idPages => $menuSectionName) {
            $itemsLinks = $this->getItemsLinks($menuSectionName, $idPages);

            $subLinks = array();
            foreach ($itemsLinks as $link) {
                $subLinks[$link->value] =  $link->title;
            }

            $links[ucfirst($menuSectionName)] = $subLinks;
        }

        return $links;
    }


    private function getMenuLinks()
    {
        $address = 'http://' . $this->hostname .'/';

        return $this->database->query("
        SELECT menu_title AS title, CONCAT('$address', slug) as value
        FROM pages
        WHERE in_menu = 1
        ORDER BY sort")->fetchAll();
    }


    private function getLinkableItems()
    {
        return $this->database->query("
        SELECT id_pages, pages.slug
        FROM pages
            JOIN items USING(id_pages)
        	JOIN items_type USING(id_items_type)
        WHERE is_linkable = 1
        GROUP BY id_pages
        ORDER BY sort")->fetchPairs();
    }

    /**
     * @param $postType string
     * @param $tableName string
     */
    private function getItemsLinks($menuSectionName, $idPages)
    {
        $address = 'http://' . $this->hostname .'/' . $menuSectionName . '/';

        return $this->database->query("
        SELECT title, CONCAT('$address', slug) as value
        FROM items
        	JOIN items_type USING(id_items_type)
        WHERE id_pages = $idPages AND is_linkable = 1
        ORDER BY date_created DESC")->fetchAll();
    }


    /**
     * @param $jsonLinks string
     */
    private function saveLinksToFile($jsonLinks)
    {
        file_put_contents('../www/js/links.json', $jsonLinks);
    }
}

