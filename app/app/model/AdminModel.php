<?php

namespace App\Model;

use Nette,
    Nette\Security\Passwords,
    Nette\Database\UniqueConstraintViolationException;


class AdminModel extends Nette\Object
{
    /** @var Nette\Database\Context */
    private $database;

    /** @var RouterCacheManager */
    private $routerCacheManager;

    /** @var AlbumsModel */
    private $galleryModel;


    const BLOCK_USER = 1;
    const UNBLOCK_USER = 0;


    public function __construct(Nette\Database\Context $database, RouterCacheManager $routerCacheManager, AlbumsModel $galleryModel)
    {
        $this->database = $database;
        $this->routerCacheManager = $routerCacheManager;
        $this->galleryModel = $galleryModel;
    }

    public function getPagesType()
    {
        return $this->database->query('
        SELECT id_pages_type, name
        FROM pages_type
        ORDER BY name')->fetchPairs();
    }

    public function getPageSlug($id)
    {
        return $this->database->query('
        SELECT slug
        FROM pages
        WHERE id_pages = ?',$id)->fetchField(0);
    }


    /************************* METODY PRO VALIDACI FORMULARE *******************/

    public function pageTitleExists($pageTitle, $updatedPageId)
    {
        $baseQuery = '
            SELECT id_pages
            FROM pages
            WHERE title = ?';

        if ($updatedPageId) {
            return $this->database->query("
            $baseQuery AND id_pages != ?", $pageTitle, $updatedPageId)->fetchField();

        } else {
            return $this->database->query("
            $baseQuery", $pageTitle)->fetchField();
        }
    }

    public function menuTitleExists($menuTitle, $updatedPageId)
    {
        $baseQuery = '
            SELECT id_pages
            FROM pages
            WHERE menu_title = ?';

        if ($updatedPageId) {
            return $this->database->query("
            $baseQuery AND id_pages != ?", $menuTitle, $updatedPageId)->fetchField();

        } else {
            return $this->database->query("
            $baseQuery", $menuTitle)->fetchField();
        }
    }

    public function pageSlugExists($pageSlug, $updatedPageId)
    {
        $baseQuery = '
            SELECT id_pages
            FROM pages
            WHERE slug = ?';

        if ($updatedPageId) {
            return $this->database->query("
            $baseQuery AND id_pages != ?", $pageSlug, $updatedPageId)->fetchField();

        } else {
            return $this->database->query("
            $baseQuery", $pageSlug)->fetchField();
        }
    }



    public function getHomepageId()
    {
        return $this->database->query('SELECT id_pages FROM pages WHERE homepage = 1')->fetchField();
    }

    //vrátí TRUE v případě, že se jedná o domovskou stránku
    public function isHomepage($idPages)
    {
        $homepage = $this->database->query('SELECT homepage FROM pages WHERE id_pages = ?', $idPages)->fetchField();
        return ($homepage == 1);
    }

    //přidá stránku. Pokud má být nová stránka nastavená jako homepage, současná homepage přestane platit
    //pořadí - stránka je prvně přidána na konec menu
    public function addPage($values)
    {
        if ($values['homepage']) {
            $this->database->query('UPDATE pages SET homepage = 0 WHERE homepage = 1');
            $this->database->query('UPDATE pages SET sort = sort + 1 WHERE sort >= 1');
            $values['sort'] = 1; // homepage musi byt vzdy prvni sekce v poradi

            $this->routerCacheManager->removeHomepage();

        } else {
            $sortLast = $this->database->query('SELECT MAX(sort)+1 FROM pages')->fetchField();
            $values['sort'] = $sortLast;
        }

        $this->database->query('INSERT INTO pages ', $values);
    }

    //úprava stránky. Tímto způsobem nelze měnit pořadí stránek, na to existují jiné metody
    public function updatePage($values, $idPages)
    {
        if (isset($values['homepage'])) {

            if ($values['homepage'] && !$this->isHomepage($idPages)) {
                $this->database->query('UPDATE pages SET homepage = 0 WHERE homepage = 1');
                $this->setSort($idPages, 1); // homepage musi byt vzdy prvni sekce v poradi

                $this->routerCacheManager->removeHomepage();
            }

            // pokud by se stalo, ze upravovana stranka je homepage a uzivatel u ni tuto vlastnost odnastavil
            if (!$values['homepage'] && $this->isHomepage($idPages)) {
                $values['homepage'] = 1;
            }
        }

        $this->database->query('UPDATE pages SET ? WHERE id_pages = ?', $values, $idPages);

        $this->routerCacheManager->removePageSlug();
    }

    // TODO Upravit - stranku nemazat, pouze "znepristupnit"
    public function deletePage($idPages)
    {
        $sort = $this->database->query('SELECT sort FROM pages WHERE id_pages = ?', $idPages)->fetchField();


        /*$idPageType = $this->database->query('
        SELECT id_pages_type
        FROM pages
        WHERE id_pages = ?', $idPages)->fetchField();

        if ($idPageType === 4) {

            $albums = $this->database->query('
            SELECT id_albums
            FROM albums
            WHERE id_pages = ?', $idPages)->fetchAll();

            if ($albums) {
                foreach ($albums as $album) {
                    $this->galleryModel->deleteAlbum($album->id_albums);
                }
            }

        } else {
            $this->database->query("DELETE FROM photos WHERE id_albums IN (SELECT id_albums FROM albums WHERE id_pages = ?)", $idPages);
            $this->database->query("DELETE FROM albums WHERE id_pages = ?", $idPages);
        }


        $this->database->query('DELETE FROM pages WHERE id_pages = ?', $idPages);*/
        $this->database->query('UPDATE pages SET sort = sort - 1 WHERE sort > ? ORDER BY sort ASC', $sort);

        $this->routerCacheManager->removePageSlug();

        if ($this->isHomepage($idPages))
            $this->routerCacheManager->removeHomepage();
    }


    public function getPages()
    {
        return $this->database->query('SELECT * FROM pages ORDER BY sort')->fetchAll();
    }

    public function getPage($idPages)
    {
        return $this->database->query('SELECT * FROM pages WHERE id_pages = ?', $idPages)->fetch();
    }


    //nastaví pořadí stránky na požadovanou hodnotu. Navíc zajišťuje unikátnost sloupce sort (tj. že na jednom pořadovém čísle nebudou dvě a více stránek
    public function setSort($idPages, $sort)
    {
        $sortCurrent = $this->database->query('SELECT sort FROM pages WHERE id_pages = ?', $idPages)->fetchField();
        $this->database->query('UPDATE pages SET sort = -1 WHERE id_pages = ?', $idPages);

        //stránka bude mít nižší pořadové číslo než doposud
        if ($sort < $sortCurrent) {
            $this->database->query('UPDATE pages SET sort = sort + 1 WHERE sort >= ? AND sort < ? ORDER BY sort DESC', $sort, $sortCurrent);
            //stránka bude mít vyšší pořadové číslo než doposud
        } else if ($sort > $sortCurrent) {
            $this->database->query('UPDATE pages SET sort = sort - 1 WHERE sort > ? AND sort <= ? ORDER BY sort ASC', $sortCurrent, $sort);
        }

        $this->database->query('UPDATE pages SET sort = ? WHERE id_pages = ?', $sort, $idPages);
        //pokud je pořadové číslo stejné jako doposud, nic se nestane
    }



    /********************* SPRAVA UZIVATELU *********************/

    /**
     * Vraci zaznamy vsech uzivatelu v systemu.
     * @return array|Nette\Database\IRow[]
     */
    public function getUsers()
    {
        return $this->database->query('
        SELECT *
        FROM users
        ORDER BY blocked, username')->fetchAll();
    }

    /**
     * Vraci vybrane udaje o uzivateli.
     * @param $idUsers int
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function getUser($idUsers)
    {
        return $this->database->query('
        SELECT id_users, username, name, surname, blocked
        FROM users
        WHERE id_users = ?', $idUsers)->fetch();
    }

    /**
     * Umoznuje zmenu udaju o uzivateli.
     * @param $values Nette\Utils\ArrayHash
     * @param $idUsers int
     */
    public function updateUser($values, $idUsers)
    {
        $this->database->query('
        UPDATE users
        SET ?
        WHERE id_users = ?', $values, $idUsers);
    }

    /**
     * Umoznuje zmenu hesla.
     * @param $values Nette\Utils\ArrayHash
     * @param $idUsers int int
     */
    public function updatePassword($values, $idUsers)
    {
        $password = Passwords::hash($values->password);
        $this->database->query('
        UPDATE users
        SET password = ?
        WHERE id_users = ?', $password, $idUsers);
    }

    /**
     * Umoznuje blokovat a odblokovat zadaneho uzivatele.
     * @param $action int
     * @param $idUsers int
     */
    public function switchBlockingUser($action, $idUsers)
    {
        $this->database->query('
        UPDATE users
        SET blocked = ?
        WHERE id_users = ?', $action, $idUsers);
    }

    /**
     * Prida noveho uzivatele.
     * @param $values Nette\Utils\ArrayHash
     * @return void
     * @throws UniqueConstraintViolationException
     */
    public function addUser($values)
    {
        try {
            $this->database->query('
			INSERT INTO users ',
                array(
                    'username' => $values->username,
                    'name' => $values->name,
                    /*'surname' => $values->surname,*/
                    'password' => Passwords::hash($values->password),
                    'id_user_roles' => 2,
                )
            );
        } catch (UniqueConstraintViolationException $e) {
            throw new UniqueConstraintViolationException('Zadané uživatelské jméno již existuje !');
        }
    }

    /**
     * Vraci, jestli je jiz v DB uzivatel s predanym uzivatelskym jmenem.
     * @param $username string
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function usernameExists($username)
    {
        return $this->database->query('
		SELECT * FROM users
		WHERE UPPER(username) = UPPER( ? )', $username)->fetch();
    }

}
