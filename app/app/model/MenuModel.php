<?php

namespace App\Model;

use Nette;


class MenuModel extends Nette\Object
{
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

    public function getMenu()
    {
        return $this->database->query('
		SELECT id_pages, menu_title, slug, name AS page_type, in_menu
		FROM pages
		  JOIN pages_type USING (id_pages_type)
		WHERE in_menu = 1
		ORDER BY sort')->fetchAll();
    }

    public function getActivePageId($slugId)
    {
        $activePageId = $this->database->query('
        SELECT id_pages
        FROM items
        WHERE id_items = ?', $slugId)->fetchField();

        return $activePageId;
    }


    public function getLatestNews()
    {
        $weekAgo = new \DateTime("-1 week");

        return $this->database->query("
        SELECT id_news, date
        FROM news
          JOIN items USING (id_items)
        WHERE date <= NOW() AND date >= ?", $weekAgo)->fetchPairs();
    }


    public function getSlides()
    {
        return $this->database->query('
		SELECT perex, title, slug
		FROM news
          JOIN items USING (id_items)
		WHERE in_slideshow AND date_created <= NOW()
		ORDER BY date_created DESC
		LIMIT 2')->fetchAll();
    }
}
