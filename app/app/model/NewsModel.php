<?php

namespace App\Model;

use Nette;


class NewsModel extends BaseItemsModel
{
    const IMAGE_COLUMN_NAME = 'news_thumb';


    public function __construct(
        $dbname,
        Nette\Database\Context $database,
        RouterCacheManager $routerCacheManager,
        LinksGenerator $linksGenerator,
        Nette\Security\User $user
    ) {
        parent::__construct($dbname, $database, $routerCacheManager, $linksGenerator, $user);
    }


    /**
     * @param $idNews
     * @return bool|Nette\Database\IRow|Nette\Database\Row
     */
    public function getNews($idNews)
    {
        return $this->database->query('
		SELECT *
		FROM news
		    JOIN items USING(id_items)
		WHERE id_news = ?',$idNews)->fetch();
    }

    /**
     * Vrátí všechny příspěvky patřící k dané stránce.
     * @param $paginator Nette\Utils\Paginator
     * @param bool $admin
     * @return array|Nette\Database\IRow[]
     */
    public function getAllNews($paginator, $admin = false)
    {
        // pouze adminovi se muze zobrazit novinka, ktera jeste "nevysla"
        $dateLimit = $admin ? '' : 'WHERE `date` <= NOW()';

        $offset = $paginator->getOffset();
        $length = $paginator->getLength();

        return $this->database->query("
		SELECT id_news, id_items, title, slug, `date`, news_thumb, perex, id_pages
		FROM news
          JOIN items USING (id_items)
		$dateLimit
        ORDER BY `date` DESC
        LIMIT $offset, $length")->fetchAll();
    }

    public function getNewsCount()
    {
        return $this->database->query("
		SELECT COUNT(*)
		FROM news")->fetchField();
    }

    protected function getSlug($title)
    {
        $idItems = $this->getNextIdItems();
        $slug = $idItems . '-' . substr(Nette\Utils\Strings::webalize($title), 0, 50);
        return $slug;
    }

    /*************** INSERT, UPDATE A DELETE OPERACE ***************/

    public function addNews($values)
    {
        $values = $this->addItem($values);

        $this->database->query('INSERT INTO news ', $values);
     }

    public function updateNews($values, $idNews)
    {
        $values = $this->updateItem($values);

        //pokud nahrávám nový obrázek, starý vymažu, pokud existuje
        if (isset($values[self::IMAGE_COLUMN_NAME])) {
            $this->removeImage($idNews, self::IMAGE_COLUMN_NAME);
        }

        $this->database->query('UPDATE news SET ? WHERE id_news = ?', $values, $idNews);
    }

    public function deleteNews($idNews)
    {
        $idItems = $this->getIdItems($idNews);

        $this->database->query('DELETE FROM news WHERE id_news = ?', $idNews);

        $this->deleteItem($idItems);
    }
}
