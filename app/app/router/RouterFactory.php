<?php

namespace App;

use Nette,
    App\Model\RouterCacheManager,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter(RouterCacheManager $routerCacheManager)
	{
        $homepageId = $routerCacheManager->getHomepageId();

        $router = new RouteList();

        // Pokud je zadany index.html/index.htm/index.php preskoci na vychozi stranku.
        $router[] = new Route('index<? \.html?|\.php|>', array(
            'presenter' => 'Page',
            'action' => 'default',
            'slug' => $homepageId
        ), Route::ONE_WAY);


        // Vychozi stranka
        $router[] = new Route('', array(
            'presenter' => 'Page',
            'action' => 'default',
            'slug' => $homepageId
        ));


        /*
         * Routa pro posty (clanky/eventy/alba/...).
         * Musi zacinat cislem nasledovanym spojovnikem (např. 1-titulek)
         */
        $router[] = new Route('<itemType>/<itemSlug [0-9]*-.+>[/<action>]', array(

            'presenter' => 'Post',

            'action' => 'default',

            NULL => array(
                // Parametr predavany do presenteru
                Route::FILTER_IN => function(array $params) use ($routerCacheManager) {
                    $params['itemType'] = $routerCacheManager->getId($params['itemType'], RouterCacheManager::PAGE_SLUGS);
                    $params['itemSlug'] = $routerCacheManager->getId($params['itemSlug'], RouterCacheManager::ITEMS_SLUG);
                    return $params;
                },
                // Parametr v url
                Route::FILTER_OUT => function(array $params) use ($routerCacheManager) {
                    $params['itemSlug'] = $routerCacheManager->getSlug($params['itemSlug'], RouterCacheManager::ITEMS_SLUG);
                    $params['itemType'] = $routerCacheManager->getSlug($params['itemType'], RouterCacheManager::PAGE_SLUGS);
                    return $params;
                }
            ),

        ));


        /*
         * Routa pro ostatni stranky.
         * Nesmi zacinat cislici !
         * Altenativa: //[<action>.]localhost/%basePath%/[<slug>]
         */
        $router[] = new Route('[<slug>][/<action>]', array(

            'presenter' => 'Page',

            'action' => 'default',

            // Meni ID stranek na slugy a naopak.
            'slug' => array(
                // Parametr predavany do presenteru
                Route::FILTER_IN => function($slug) use ($routerCacheManager) {
                    return $routerCacheManager->getId($slug, RouterCacheManager::PAGE_SLUGS);
                },
                // Parametr v url
                Route::FILTER_OUT => function($id) use ($routerCacheManager) {
                    return $routerCacheManager->getSlug($id, RouterCacheManager::PAGE_SLUGS);
                },
            ),
        ));


        /* Sem by to v podstate nemelo nikdy spadnout, ale stejne to zde musi byt!
         * Jinak dojde k chybe pri hledani odkazu, ktere vedou z ErrorPresenteru.
         * Napr. u action "logout".
         */
        $router[] = new Route('<presenter>[/<exception>][/<action>]', array(
            'presenter' => 'Error',
            'action' => 'default',
            'exception' => '4xx'
        ));


        return $router;
    }

}
