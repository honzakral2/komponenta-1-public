<?php

namespace App\Utils;

use Nette;


class ImageHandler extends Nette\Object
{
    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var \App\Model\BaseItemsModel */
    private $imagesDir;

    /** @var Nette\Application\UI\Form */
    private $form;


    public function __construct($form, $imagesDir, $width, $height)
    {
        $this->form = $form;
        $this->setImagesDir($imagesDir);

        $this->width = $width;
        $this->height = $height;
    }


    /**
     * @param $file Nette\Http\FileUpload
     * @return string
     */
    public function getImageName($file)
    {
        if ($this->isImageValid($file)) {
            $sanitizedName = $file->getSanitizedName();
            $randomString = Nette\Utils\Random::generate(20);
            $fileExt = mb_substr($sanitizedName, strrpos($sanitizedName, "."));
            $imageName = $randomString . $fileExt;

            $image = $file->toImage();
            $image->save($this->imagesDir . '/' . $imageName, 75);

            $this->resizeImage($image);
            $image->save($this->imagesDir . '/thumbs/' . $imageName, 80);

            return $imageName;
        }

        return '';
    }

    /**
     * Vytvori nahled obrazku.
     * @param $image Nette\Utils\Image
     * @internal param string $imageName
     */
    private function resizeImage($image)
    {
        if ($image->width > $image->height) {
            $image->resize(NULL, $this->height);
            $image->crop('50%', 0, $this->width, $this->height);
        } else {
            $image->resize($this->width, NULL);
            $image->crop(0, '50%', $this->width, $this->height);
        }
    }

    private function setImagesDir($dirPath)
    {
        $this->makeDir($dirPath);

        $thumbDirPath = $dirPath . "/thumbs";
        $this->makeDir($thumbDirPath);

        $this->imagesDir = $dirPath;
    }

    /**
     * @param $dir string
     */
    private function makeDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    /**
     * @param $file Nette\Http\FileUpload
     * @return bool
     */
    private function isImageValid($file)
    {
        $isValid = false;
        $presenter = $this->form->getPresenter();
        $errorMsg = 'Soubor "' . $file->name . '" se nepodařilo nahrát. ';

        if ($file->getError() === 1) {
            $presenter->flashMessage(
                $errorMsg . 'Přesáhl maximální hostingem povolenou velikost
                uploadovaného souboru (tj. ' . ini_get('upload_max_filesize') . ' B).',
                'error'
            );

        } elseif (!$file->isOk()) {
            $presenter->flashMessage($errorMsg . 'Došlo k neočekávané chybě.', 'error');

        } elseif (!$file->isImage()) {
            $presenter->flashMessage($errorMsg . 'Není ve formátu JPG, PNG nebo GIF.', 'error');

        } elseif (!$this->isSizeOk($file)) {
            $presenter->flashMessage($errorMsg . 'Má menší rozměry než jsou požadované.', 'error');

        } else {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Kontrola velikosti uploadovanych fotek.
     * @param $file Nette\Http\FileUpload
     * @return bool
     */
    private function isSizeOk($file)
    {
        $imageSize = $file->getImageSize();
        return ( $imageSize['0'] >= $this->width || $imageSize['1'] >= $this->height );
    }
}
