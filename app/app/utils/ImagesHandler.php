<?php

namespace App\Utils;

use Nette;


class ImagesHandler extends Nette\Object
{
    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var array */
    private $fileNames;

    /** @var Nette\Application\UI\Form */
    private $form;

    /** @var string */
    private $imagesDir;

    /** @var boolean */
    private $avoidDuplication;

    /** @var int */
    private $duplicatesCount;


    /**
     * @param $form Nette\Application\UI\Form
     * @param $imagesDir
     * @param $width int
     * @param $height int
     */
    public function __construct($form, $imagesDir, $width, $height)
    {
        $this->form = $form;
        $this->setImagesDir($imagesDir);

        $this->width = $width;
        $this->height = $height;

        $this->fileNames = array();
        $this->avoidDuplication = false;
        $this->duplicatesCount = 0;
    }

    /**
     * Vrati pole s nazvy vlozenych souboru.
     * @param $files array
     * @return array
     */
    public function getImageNames($files)
    {
        if ($files) {
            $this->processImages($files);

        }

        return $this->fileNames;
    }

    /**
     * @param $files array
     */
    private function processImages($files)
    {
        /** @var $file Nette\Http\FileUpload */
        foreach ($files as $file) {

            $imageName = $file->getSanitizedName();
            $filePath = $this->imagesDir . '/' . $imageName;

            if ($this->checkIfExists($filePath)) {
                $this->duplicatesCount++;

            } elseif ($this->isImageValid($file)) {

                $image = $file->toImage();
                $image->save($filePath, 75);

                $this->resizeImage($image);
                $image->save($this->imagesDir . '/thumbs/' . $imageName, 80);

                $this->fileNames[] = $imageName;
            }
        }
    }

    /**
     * @param $file Nette\Http\FileUpload
     * @return bool
     */
    private function isImageValid($file)
    {
        $isValid = false;
        $presenter = $this->form->getPresenter();
        $errorMsg = 'Soubor "' . $file->name . '" se nepodařilo nahrát. ';

        if ($file->getError() === 1) {
            $presenter->flashMessage(
                $errorMsg . 'Přesáhl maximální hostingem povolenou velikost
                uploadovaného souboru (tj. ' . ini_get('upload_max_filesize') . ' B).',
                'error'
            );

        } elseif (!$file->isOk()) {
            $presenter->flashMessage($errorMsg . 'Došlo k neočekávané chybě.', 'error');

        } elseif (!$file->isImage()) {
            $presenter->flashMessage($errorMsg . 'Není ve formátu JPG, PNG nebo GIF.', 'error');

        } elseif (!$this->isSizeOk($file)) {
            $presenter->flashMessage($errorMsg . 'Má menší rozměry než jsou požadované.', 'error');

        } else {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Vytvori nahled obrazku.
     * @param $image Nette\Utils\Image
     * @internal param string $imageName
     */
    private function resizeImage($image)
    {
        if ($image->width > $image->height) {
            $image->resize(NULL, $this->height);
            $image->crop('50%', 0, $this->width, $this->height);
        } else {
            $image->resize($this->width, NULL);
            $image->crop(0, '50%', $this->width, $this->height);
        }
    }

    private function setImagesDir($dirPath)
    {
        $this->makeDir($dirPath);

        $thumbDirPath = $dirPath . "/thumbs";
        $this->makeDir($thumbDirPath);

        $this->imagesDir = $dirPath;
    }

    /**
     * Jestlize dana slozka neexistuje, vytvori se.
     * @param $dirPath string
     */
    private function makeDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            mkdir($dirPath, 0777, true);
        }
    }

    /**
     * Kontrola velikosti uploadovanych fotek.
     * @param $file Nette\Http\FileUpload
     * @return bool
     */
    private function isSizeOk($file)
    {
        $imageSize = $file->getImageSize();
        return ( $imageSize['0'] >= $this->width || $imageSize['1'] >= $this->height );
    }

    /**
     * Kontrola, jestli jiz vkladana fotka neni v albu.
     * Defaultne je vypnuta.
     * @param $filePath string
     * @return bool
     */
    private function checkIfExists($filePath)
    {
        return $this->avoidDuplication ? file_exists($filePath) : false;
    }

    /**
     * Umoznuje aktivovat kontrolu, jestli jiz vkladana fotka neni v albu.
     * @param $enable boolean
     */
    public function avoidDuplication($enable)
    {
        $this->avoidDuplication = $enable;
    }

    /**
     * Vrati pocet fotek, ktere jiz v albu jsou.
     * @return int
     */
    public function getDuplicatesCount()
    {
        return $this->duplicatesCount;
    }

}
