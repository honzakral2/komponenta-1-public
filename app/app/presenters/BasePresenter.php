<?php

namespace App\Presenters;

use Nette,
    App\Components\SignInFormFactory,
    App\Components\AdminControl,
    App\Components\MenuControl;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var SignInFormFactory @inject */
    public $signInFormFactory;

    /** @var MenuControl @inject */
    public $menuControl;

    /** @var AdminControl @inject */
    public $adminControl;


    protected function startup()
    {
        parent::startup();

        // Logout
        if ($this->getAction() == 'logout'){
            $this->user->logout(TRUE);
            $this->flashMessage('Odhlášení proběhlo úspěšně.');
            $this->redirect('default');
        }

        /*  Nutne nastavit kvuli beforeRender(). At uz totiz uzivatel bude tukat na misto v adrese,
            ktere reprezentuje action cokoliv, stejne se mu zobrazi stranka. Proto omezime moznosti
            na "admin" a "default" view.
        */
        if ($this->getView() != 'admin' && $this->getView() != 'default')
            throw new Nette\Application\BadRequestException(); // chyba 404

        $this->authorization();
    }


    /*************************** AUTORIZACE ***************************/

    /**
     * Metoda zajistujici autorizaci.
     * @throws Nette\Application\ForbiddenRequestException
     */
    private function authorization() {
        if( !$this->user->isAllowed($this->getName(), $this->getAction())
            || ( $this->getSignal() !== NULL && !$this->user->isAllowed($this->getName(), $this->formatSignalString()) ) ){
            throw new Nette\Application\ForbiddenRequestException(); // chyba 403
        }
    }

    /**
     * Umoznuje resit autorizaci i u signalu.
     * @return null|string
     */
    protected function formatSignalString()
    {
        return $this->getSignal() === NULL ? NULL : ltrim(implode('-', $this->getSignal()), '-') . '!';
    }


    /************* KOMPONENTY SPOLECNE PRO VSECHNY STRANKY ************/

    /**
     * Menu komponenta.
     * @return MenuControl
     */
    protected function createComponentMenu()
    {
        return $this->menuControl;
    }

    /**
     * Prihlasovaci formular.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = $this->signInFormFactory->create();

        /* Workaround kvuli ErrorPresenteru
         * zdroj: https://forum.nette.org/cs/2984-errorpresenter-v-nem-komponenta-s-formularem
         */
        $form->setAction($this->getPresenter()->link('default', array('do' => 'signInForm-submit')));

        return $form;
    }

    /**
     * Komponenta s administracnim rozhranim.
     * @return AdminControl
     */
    protected function createComponentAdmin()
    {
        return $this->adminControl;
    }


    // TODO Umoznit upload vsech souboru (tzn. ne jen obrazku)
    // TODO Dat tuto metodu nekam jinam a odstranit $this->getView() === 'admin' z podminky
    public function handleFileUpload()
    {
        if ($this->isAjax() && $this->getView() === 'admin') {

            $payload = $this->getPayload();
            $payload->error = '';
            $payload->filepath = '';

            /** @var $file Nette\Http\FileUpload */
            $file = $this->getRequest()->getFiles()['tinyMCEFile'];
            $maxSize = \Helpers::maxSizeInBytes('post_max_size');
            $extensions = array('JPG', 'JPEG', 'PNG', 'GIF');

            if ($maxSize > 0 && $maxSize < $_SERVER['CONTENT_LENGTH']) {
                $payload->error = 'Zvolený soubor přesáhl povolenou velikost (' . number_format($maxSize, 0 , ',', ' ') . ' bajtů).';
            } elseif (!$file->isImage()) {
                $payload->error = 'Zvolený soubor není obrázek ve formátu JPG, PNG nebo GIF.';
            } elseif (!in_array(strtoupper(pathinfo($file->getName(), PATHINFO_EXTENSION)), $extensions)) {
                $payload->error = 'Koncovka souboru musí být jedna z: ' . implode(', ', $extensions);
            } elseif (!$file->isOk()) {
                $payload->error = 'Při nahrávání zvoleného souboru nastal neočekávaný problém .';
            }

            if (!$payload->error) {
                $filename = $file->getSanitizedName(); // TODO Zmenit nazev uploadovaneho souboru
                $filePath = '/images/' . $filename;
                $file->move('.' . $filePath);

                $payload->filepath = $filePath;
                $payload->filename = $filename;
                $this->flashMessage('Zvolený obrázek byl úspěšně uploadován.');

            } else {
                $this->flashMessage($payload->error, 'warning');
            }

            //$this->sendPayload();
        } else {
            $this->redirect('this');
        }
    }



    /**
     * Umozni automatizovane vykreslit flash zpravu.
     */
    public function afterRender()
    {
        if ($this->hasFlashSession() && $this->isAjax()) {
            $this->redrawControl('flashMessage');
        }
    }
}
