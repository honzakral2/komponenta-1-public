<?php

namespace App\Presenters;

use App\Components\AlbumControl;
use App\Components\HomepageControl;
use Nette,
    App\Model,
    App\Model\PageModel,
    App\Components\NewsControl,
    App\Components\GalleryControl,
    App\Components\PageContentFormFactory,
    App\Components\ConcertsControl,
    App\Components\ReferencesControl,
    App\Components\MusiciansControl,
    App\Components\ContactsControl,
    App\Model\VideoManager;


/**
 * Page presenter.
 */
class PagePresenter extends BasePresenter
{
    /** @persistent */
    public $slug;


    /** @var PageModel @inject */
    public $pageModel;

    /** @var HomepageControl @inject */
    public $homepageControl;

    /** @var PageContentFormFactory @inject */
    public $pageContentFormFactory;

    /** @var NewsControl @inject */
    public $pageControl;

    /** @var ConcertsControl @inject */
    public $concertsControl;

    /** @var ReferencesControl @inject */
    public $referencesControl;

    /** @var MusiciansControl @inject */
    public $musiciansControl;

    /** @var GalleryControl @inject */
    public $galleryControl;

    /** @var AlbumControl @inject */
    public $albumControl;

    /** @var ContactsControl @inject */
    public $contactsControl;


    protected function startup()
    {
        parent::startup();

        /*  Zkontroluju, jestli sekce s danym slugem existuje,
            pokud ne => presmerovani na 404.
            Ze je slug NULL, kdyz sekce neexistuje, zaridi prislusny router
            v RouterFactory.
        */
        if ($this->slug === NULL) {
            throw new Nette\Application\BadRequestException();
        }
    }

    /**
     * Nastavim stejnou sablonu pro vsechny view tohoto presenteru.
     */
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->setFile(__DIR__ . '/templates/Page/default.latte');

        $page = $this->pageModel->getPageContent($this->slug);
        $this->template->page = $page;
        $pageType = $this->pageModel->getPageType($this->slug);
        $this->template->pageType = $pageType;

        if ($this->getView() === 'admin') {
            $page['id_pages'] = $this->slug;
            $this['pageContentForm']->setValues($page);
        }

        if ($pageType === 'gallery') {
            $videoManager = new VideoManager();
            $this->template->videos = $videoManager->getVideos();
        }
    }


    /****************** VYTVARENE KOMPONENTY ******************/

    protected function createComponentHomepage()
    {
        return $this->homepageControl;
    }


    protected function createComponentPageContentForm()
    {
        return $this->pageContentFormFactory->create();
    }


    protected function createComponentPosts()
    {
        return $this->pageControl;
    }

    protected function createComponentConcerts()
    {
        return $this->concertsControl;
    }

    protected function createComponentReferences()
    {
        return $this->referencesControl;
    }

    protected function createComponentMusicians()
    {
        return $this->musiciansControl;
    }

    protected function createComponentContacts()
    {
        return $this->contactsControl;
    }

    /****************** GALERIE & ALBUM ******************/

    protected function createComponentGallery()
    {
        return $this->galleryControl;
    }

    protected function createComponentAlbum()
    {
        return $this->albumControl;
    }
}
