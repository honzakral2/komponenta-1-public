<?php

namespace App\Presenters;

use App\Components\AlbumControl;
use App\Components\ConcertFormFactory;
use App\Components\ReferenceFormFactory;
use Nette,
    App,
	App\Model,
    App\Model\ItemsModel,
    App\Components\NewsFormFactory,
    App\Components\CommentsControl;


/**
 * Post presenter.
 */
class PostPresenter extends BasePresenter
{
    /** @persistent */
    public $itemSlug;

    /** @persistent */
    public $itemType;


    /** @var ItemsModel @inject */
    public $itemsModel;


    /** @var NewsFormFactory @inject */
    public $newsFormFactory;

    /** @var ConcertFormFactory @inject */
    public $concertFormFactory;

    /** @var ReferenceFormFactory @inject */
    public $referenceFormFactory;


    /** @var AlbumControl @inject */
    public $albumControl;

    /** @var CommentsControl @inject */
    public $commentsControl;


    protected function startup()
    {
        parent::startup();

        /*  Zkontroluju, jestli sekce s danym slugem existuje,
            pokud ne => presmerovani na 404.
            Ze je slug NULL, kdyz sekce neexistuje, zaridi prislusny router
            v RouterFactory.
        */
        if ($this->itemSlug === NULL || $this->itemType === NULL) {
            throw new Nette\Application\BadRequestException();
        }
    }


    public function beforeRender()
    {
        parent::beforeRender();

        $itemType = $this->itemsModel->getItemType($this->itemSlug);

        $this->template->setFile(__DIR__ . '/templates/Post/' . $itemType . '.latte');

        $item = $this->itemsModel->getItem($this->itemSlug, $itemType);
        $this->template->post = $item;

        if ($this->getView() === 'admin') {
            if (isset($item['date'])) {
                $date = new \DateTime($item['date']);
                $item['time'] = $date->format('H:i');
                $item['date'] = $date->format('d.m.Y');
            }

            $this[$itemType . "Form"]->setValues($item);
        }

        $lang = $this->getParameter('lang');
        if ($lang)
            $this->template->lang = $lang;
    }

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentNewsForm()
    {
        return $this->newsFormFactory->create();
    }

    protected function createComponentConcertsForm()
    {
        return $this->concertFormFactory->create();
    }

    protected function createComponentReferencesForm()
    {
        return $this->referenceFormFactory->create();
    }


    protected function createComponentAlbum()
    {
        return $this->albumControl;
    }


    /************************ RESENI KOMENTARU U POSTU ************************/

    /**
     * Skryje/zobrazi dany komentar.
     * @param $id int - ID komentare
     */
    public function handleSwitchCommentsAllowed()
    {
        if ($this->getPresenter()->isAjax()) {

            $comments_allowed = $this->itemsModel->getCommentAllowed($this->itemSlug);

            $action = $comments_allowed
                ? ItemsModel::DENY_COMMENTS
                : ItemsModel::ALLOW_COMMENTS;

            $this->itemsModel->switchCommentsAllowed($action, $this->itemSlug);

            $this->redrawControl('commentsControl');
        } else {
            $this->redirect('this');
        }
    }

    /**
     * @return CommentsControl
     */
    protected function createComponentComments()
    {
        return $this->commentsControl;
    }
}
