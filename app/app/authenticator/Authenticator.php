<?php

namespace App;

use Nette,
	Nette\Security\Passwords,
	Nette\Security\AuthenticationException;


class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator
{
	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	/**
	 * Provede autentizaci.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->database->query('
		SELECT 
			id_users, username, users.name, password, blocked,
			user_roles.name AS role
		FROM users
			JOIN user_roles USING(id_user_roles)
		WHERE UPPER(username) = UPPER( ? )',$username)->fetch();
		
		
		if ( !$row || !Passwords::verify($password, $row['password']) ) {
			throw new AuthenticationException('Bylo zadáno špatné uživatelské jméno nebo heslo.');

		} elseif ($row['blocked']) {
			throw new AuthenticationException('Tento účet byl zablokován. V případě námitek se obraťte na vedení webu.');

		} elseif (Passwords::needsRehash($row['password'])) {
			$row->update(array(
				'password' => Passwords::hash($password),
			));
		}
		
		$arr = (array)$row;
		unset($arr['password']);
		unset($arr['id_users']);
		unset($arr['role']);
		return new Nette\Security\Identity($row['id_users'], $row['role'], $arr);
	}
}